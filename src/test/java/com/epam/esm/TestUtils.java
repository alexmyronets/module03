package com.epam.esm;

import java.io.IOException;
import java.util.Objects;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public abstract class TestUtils {

    private TestUtils() {
    }

    public static String getJsonString(String path, Class<?> clazz) throws IOException {
        return new String(Objects.requireNonNull(clazz
                                                     .getClassLoader()
                                                     .getResourceAsStream(path))
                                 .readAllBytes());
    }

    public static String getUpdateBaseURL(String updateURL, String jsonBody) {
        return jsonBody.replaceAll("http://localhost:8080", updateURL);
    }

    public static String getBaseUrl(MockMvc mockMvc) {
        return MockMvcRequestBuilders.post("")
                                     .buildRequest(mockMvc.getDispatcherServlet()
                                                          .getServletContext())
                                     .getRequestURL()
                                     .toString();
    }
}
