package com.epam.esm.DTOConstants;

import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.dto.TagDTO;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class GiftCertificateDTOConstants {

    public static final GiftCertificateDTO SAVED_GIFT_CERT_DTO
        = new GiftCertificateDTO(51L,
                                 "cert51Name",
                                 "cert51Description",
                                 new BigDecimal("50.00"),
                                 (short) 90,
                                 LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                 LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(51L, "tag51"), new TagDTO(42L, "tag42"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_1
        = new GiftCertificateDTO(1L,
                                 "cert1",
                                 "description1",
                                 new BigDecimal("100.00"),
                                 (short) 90,
                                 LocalDateTime.of(2022, 12, 30, 15, 7, 30, 965765000),
                                 LocalDateTime.of(2022, 12, 30, 15, 7, 30, 965765000),
                                 new HashSet<>(Arrays.asList(new TagDTO(32L, "tag32"),
                                                             new TagDTO(26L, "tag26"),
                                                             new TagDTO(21L, "tag21"),
                                                             new TagDTO(24L, "tag24"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_2
        = new GiftCertificateDTO(2L,
                                 "cert2",
                                 "description2",
                                 new BigDecimal("150.00"),
                                 (short) 30,
                                 LocalDateTime.of(2022, 12, 30, 15, 25, 5, 523600000),
                                 LocalDateTime.of(2022, 12, 30, 15, 25, 5, 523600000),
                                 new HashSet<>(Arrays.asList(new TagDTO(26L, "tag26"),
                                                             new TagDTO(8L, "tag8"),
                                                             new TagDTO(34L, "tag34"),
                                                             new TagDTO(19L, "tag19"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_3
        = new GiftCertificateDTO(3L,
                                 "cert3",
                                 "description3",
                                 new BigDecimal("200.00"),
                                 (short) 60,
                                 LocalDateTime.of(2023, 1, 2, 18, 15, 51, 505330000),
                                 LocalDateTime.of(2023, 1, 2, 18, 15, 51, 505330000),
                                 new HashSet<>(Arrays.asList(new TagDTO(45L, "tag45"), new TagDTO(4L, "tag4"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_4
        = new GiftCertificateDTO(4L,
                                 "cert4",
                                 "description4",
                                 new BigDecimal("220.50"),
                                 (short) 120,
                                 LocalDateTime.of(2022, 1, 4, 0, 20, 46, 926409000),
                                 LocalDateTime.of(2023, 1, 4, 0, 20, 46, 926409000),
                                 new HashSet<>(Arrays.asList(new TagDTO(14L, "tag14"),
                                                             new TagDTO(28L, "tag28"),
                                                             new TagDTO(46L, "tag46"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_5
        = new GiftCertificateDTO(5L,
                                 "cert5",
                                 "description5",
                                 new BigDecimal("123.99"),
                                 (short) 180,
                                 LocalDateTime.of(2023, 1, 4, 1, 22, 25, 23598000),
                                 LocalDateTime.of(2023, 1, 25, 8, 42, 3, 777598000),
                                 new HashSet<>(Arrays.asList(new TagDTO(48L, "tag48"), new TagDTO(12L, "tag12"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_6
        = new GiftCertificateDTO(6L,
                                 "cert6",
                                 "description6",
                                 new BigDecimal("1163.00"),
                                 (short) 142,
                                 LocalDateTime.of(2022, 3, 11, 0, 0, 11, 0),
                                 LocalDateTime.of(2022, 3, 11, 15, 52, 41, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(16L, "tag16"), new TagDTO(21L, "tag21"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_7
        = new GiftCertificateDTO(7L,
                                 "cert7",
                                 "description7",
                                 new BigDecimal("2199.00"),
                                 (short) 490,
                                 LocalDateTime.of(2022, 6, 13, 3, 53, 55, 0),
                                 LocalDateTime.of(2022, 6, 13, 9, 24, 33, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(15L, "tag15"), new TagDTO(28L, "tag28"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_8
        = new GiftCertificateDTO(8L,
                                 "cert8",
                                 "description8",
                                 new BigDecimal("3094.00"),
                                 (short) 572,
                                 LocalDateTime.of(2022, 2, 3, 18, 10, 15, 0),
                                 LocalDateTime.of(2022, 2, 3, 20, 42, 55, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(36L, "tag36"),
                                                             new TagDTO(19L, "tag19"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_9
        = new GiftCertificateDTO(9L,
                                 "cert9",
                                 "description9",
                                 new BigDecimal("4056.00"),
                                 (short) 156,
                                 LocalDateTime.of(2022, 6, 17, 13, 38, 15, 0),
                                 LocalDateTime.of(2022, 6, 17, 4, 12, 14, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(14L, "tag14"),
                                                             new TagDTO(2L, "tag2"),
                                                             new TagDTO(15L, "tag15"),
                                                             new TagDTO(42L, "tag42"),
                                                             new TagDTO(46L, "tag46"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_10
        = new GiftCertificateDTO(10L,
                                 "cert10",
                                 "description10",
                                 new BigDecimal("1704.00"),
                                 (short) 583,
                                 LocalDateTime.of(2022, 2, 17, 9, 10, 44, 0),
                                 LocalDateTime.of(2022, 2, 17, 12, 35, 24, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(23L, "tag23"),
                                                             new TagDTO(3L, "tag3"),
                                                             new TagDTO(5L, "tag5"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_11
        = new GiftCertificateDTO(11L,
                                 "cert11",
                                 "description11",
                                 new BigDecimal("3938.00"),
                                 (short) 536,
                                 LocalDateTime.of(2022, 2, 24, 0, 29, 36, 0),
                                 LocalDateTime.of(2022, 2, 24, 0, 59, 24, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(25L, "tag25"), new TagDTO(37L, "tag37"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_12
        = new GiftCertificateDTO(12L,
                                 "cert12",
                                 "description12",
                                 new BigDecimal("4341.00"),
                                 (short) 165,
                                 LocalDateTime.of(2022, 3, 30, 18, 40, 44, 0),
                                 LocalDateTime.of(2022, 3, 30, 3, 35, 40, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(10L, "tag10"),
                                                             new TagDTO(18L, "tag18"),
                                                             new TagDTO(2L, "tag2"),
                                                             new TagDTO(40L, "tag40"),
                                                             new TagDTO(37L, "tag37"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_13
        = new GiftCertificateDTO(13L,
                                 "cert13",
                                 "description13",
                                 new BigDecimal("48.00"),
                                 (short) 521,
                                 LocalDateTime.of(2022, 2, 15, 4, 21, 2, 0),
                                 LocalDateTime.of(2022, 2, 15, 14, 12, 3, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(27L, "tag27"),
                                                             new TagDTO(30L, "tag30"),
                                                             new TagDTO(38L, "tag38"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_14
        = new GiftCertificateDTO(14L,
                                 "cert14",
                                 "description14",
                                 new BigDecimal("4184.00"),
                                 (short) 497,
                                 LocalDateTime.of(2022, 6, 8, 6, 58, 30, 0),
                                 LocalDateTime.of(2022, 6, 8, 2, 56, 57, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(36L, "tag36"),
                                                             new TagDTO(48L, "tag48"),
                                                             new TagDTO(1L, "tag1"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_15
        = new GiftCertificateDTO(15L,
                                 "cert15",
                                 "description15",
                                 new BigDecimal("3171.00"),
                                 (short) 587,
                                 LocalDateTime.of(2022, 5, 12, 17, 53, 43, 0),
                                 LocalDateTime.of(2022, 5, 12, 9, 35, 30, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(10L, "tag10"),
                                                             new TagDTO(45L, "tag45"),
                                                             new TagDTO(15L, "tag15"),
                                                             new TagDTO(5L, "tag5"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_16
        = new GiftCertificateDTO(16L,
                                 "cert16",
                                 "description16",
                                 new BigDecimal("2693.00"),
                                 (short) 404,
                                 LocalDateTime.of(2022, 4, 7, 15, 43, 54, 0),
                                 LocalDateTime.of(2022, 4, 7, 16, 52, 30, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(23L, "tag23"),
                                                             new TagDTO(7L, "tag7"),
                                                             new TagDTO(1L, "tag1"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_17
        = new GiftCertificateDTO(17L,
                                 "cert17",
                                 "description17",
                                 new BigDecimal("1077.00"),
                                 (short) 93,
                                 LocalDateTime.of(2022, 6, 16, 1, 35, 21, 0),
                                 LocalDateTime.of(2022, 6, 16, 15, 45, 26, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(13L, "tag13"),
                                                             new TagDTO(3L, "tag3"),
                                                             new TagDTO(38L, "tag38"),
                                                             new TagDTO(1L, "tag1"),
                                                             new TagDTO(46L, "tag46"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_18
        = new GiftCertificateDTO(18L,
                                 "cert18",
                                 "description18",
                                 new BigDecimal("1218.00"),
                                 (short) 279,
                                 LocalDateTime.of(2022, 2, 14, 2, 8, 38, 0),
                                 LocalDateTime.of(2022, 2, 14, 23, 43, 8, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(6L, "tag6"),
                                                             new TagDTO(31L, "tag31"),
                                                             new TagDTO(12L, "tag12"),
                                                             new TagDTO(5L, "tag5"),
                                                             new TagDTO(42L, "tag42"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_19
        = new GiftCertificateDTO(19L,
                                 "cert19",
                                 "description19",
                                 new BigDecimal("519.00"),
                                 (short) 79,
                                 LocalDateTime.of(2022, 6, 29, 21, 47, 21, 0),
                                 LocalDateTime.of(2022, 6, 29, 20, 53, 59, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(31L, "tag31"),
                                                             new TagDTO(34L, "tag34"),
                                                             new TagDTO(43L, "tag43"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_20
        = new GiftCertificateDTO(20L,
                                 "cert20",
                                 "description20",
                                 new BigDecimal("2417.00"),
                                 (short) 260,
                                 LocalDateTime.of(2022, 3, 3, 2, 19, 44, 0),
                                 LocalDateTime.of(2022, 3, 3, 15, 18, 57, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(35L, "tag35"),
                                                             new TagDTO(39L, "tag39"),
                                                             new TagDTO(1L, "tag1"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_21
        = new GiftCertificateDTO(21L,
                                 "cert21",
                                 "description21",
                                 new BigDecimal("4669.00"),
                                 (short) 251,
                                 LocalDateTime.of(2022, 4, 25, 7, 18, 57, 0),
                                 LocalDateTime.of(2022, 4, 25, 1, 14, 57, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(23L, "tag23"),
                                                             new TagDTO(27L, "tag27"),
                                                             new TagDTO(22L, "tag22"),
                                                             new TagDTO(31L, "tag31"),
                                                             new TagDTO(21L, "tag21"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_22
        = new GiftCertificateDTO(22L,
                                 "cert22",
                                 "description22",
                                 new BigDecimal("2671.00"),
                                 (short) 369,
                                 LocalDateTime.of(2022, 2, 25, 12, 25, 12, 0),
                                 LocalDateTime.of(2022, 2, 25, 13, 51, 14, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(2L, "tag2"),
                                                             new TagDTO(48L, "tag48"),
                                                             new TagDTO(20L, "tag20"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_23
        = new GiftCertificateDTO(23L,
                                 "cert23",
                                 "description23",
                                 new BigDecimal("245.00"),
                                 (short) 55,
                                 LocalDateTime.of(2022, 5, 30, 8, 20, 8, 0),
                                 LocalDateTime.of(2022, 5, 30, 13, 24, 50, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(39L, "tag39"),
                                                             new TagDTO(30L, "tag30"),
                                                             new TagDTO(5L, "tag5"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_24
        = new GiftCertificateDTO(24L,
                                 "cert24",
                                 "description24",
                                 new BigDecimal("130.00"),
                                 (short) 165,
                                 LocalDateTime.of(2022, 5, 12, 5, 1, 20, 0),
                                 LocalDateTime.of(2022, 5, 12, 12, 53, 32, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(31L, "tag31"),
                                                             new TagDTO(34L, "tag34"),
                                                             new TagDTO(47L, "tag47"),
                                                             new TagDTO(1L, "tag1"),
                                                             new TagDTO(24L, "tag24"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_25
        = new GiftCertificateDTO(25L,
                                 "cert25",
                                 "description25",
                                 new BigDecimal("1784.00"),
                                 (short) 455,
                                 LocalDateTime.of(2022, 3, 11, 4, 4, 28, 0),
                                 LocalDateTime.of(2022, 3, 11, 13, 59, 45, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(3L, "tag3"),
                                                             new TagDTO(47L, "tag47"),
                                                             new TagDTO(42L, "tag42"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_26
        = new GiftCertificateDTO(26L,
                                 "cert26",
                                 "description26",
                                 new BigDecimal("2688.00"),
                                 (short) 245,
                                 LocalDateTime.of(2022, 6, 9, 22, 36, 4, 0),
                                 LocalDateTime.of(2022, 6, 9, 19, 16, 17, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(48L, "tag48"), new TagDTO(8L, "tag8"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_27
        = new GiftCertificateDTO(27L,
                                 "cert27",
                                 "description27",
                                 new BigDecimal("2698.00"),
                                 (short) 346,
                                 LocalDateTime.of(2022, 6, 9, 5, 46, 21, 0),
                                 LocalDateTime.of(2022, 6, 9, 5, 0, 7, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(23L, "tag23"),
                                                             new TagDTO(15L, "tag15"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_28
        = new GiftCertificateDTO(28L,
                                 "cert28",
                                 "description28",
                                 new BigDecimal("3949.00"),
                                 (short) 98,
                                 LocalDateTime.of(2022, 4, 23, 13, 31, 35, 0),
                                 LocalDateTime.of(2022, 4, 23, 8, 16, 35, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(17L, "tag17"),
                                                             new TagDTO(22L, "tag22"),
                                                             new TagDTO(21L, "tag21"),
                                                             new TagDTO(5L, "tag5"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_29
        = new GiftCertificateDTO(29L,
                                 "cert29",
                                 "description29",
                                 new BigDecimal("1899.00"),
                                 (short) 219,
                                 LocalDateTime.of(2022, 6, 14, 16, 6, 4, 0),
                                 LocalDateTime.of(2022, 6, 14, 3, 44, 54, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(26L, "tag26"), new TagDTO(16L, "tag16"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_30
        = new GiftCertificateDTO(30L,
                                 "cert30",
                                 "description30",
                                 new BigDecimal("3870.00"),
                                 (short) 281,
                                 LocalDateTime.of(2022, 3, 1, 23, 49, 38, 0),
                                 LocalDateTime.of(2022, 3, 1, 12, 46, 39, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(6L, "tag6"),
                                                             new TagDTO(17L, "tag17"),
                                                             new TagDTO(22L, "tag22"),
                                                             new TagDTO(4L, "tag4"),
                                                             new TagDTO(33L, "tag33"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_31
        = new GiftCertificateDTO(31L,
                                 "cert31",
                                 "description31",
                                 new BigDecimal("2443.00"),
                                 (short) 249,
                                 LocalDateTime.of(2022, 6, 16, 17, 9, 17, 0),
                                 LocalDateTime.of(2022, 6, 16, 22, 27, 13, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(41L, "tag41"),
                                                             new TagDTO(44L, "tag44"),
                                                             new TagDTO(43L, "tag43"),
                                                             new TagDTO(33L, "tag33"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_32
        = new GiftCertificateDTO(32L,
                                 "cert32",
                                 "description32",
                                 new BigDecimal("4581.00"),
                                 (short) 587,
                                 LocalDateTime.of(2022, 4, 16, 0, 6, 32, 0),
                                 LocalDateTime.of(2022, 4, 16, 10, 16, 19, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(23L, "tag23"), new TagDTO(38L, "tag38"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_33
        = new GiftCertificateDTO(33L,
                                 "cert33",
                                 "description33",
                                 new BigDecimal("4770.00"),
                                 (short) 402,
                                 LocalDateTime.of(2022, 4, 22, 1, 22, 15, 0),
                                 LocalDateTime.of(2022, 4, 22, 1, 44, 18, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(10L, "tag10"), new TagDTO(3L, "tag3"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_34
        = new GiftCertificateDTO(34L,
                                 "cert34",
                                 "description34",
                                 new BigDecimal("2658.00"),
                                 (short) 221,
                                 LocalDateTime.of(2022, 4, 13, 6, 14, 17, 0),
                                 LocalDateTime.of(2022, 4, 13, 16, 18, 34, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(10L, "tag10"),
                                                             new TagDTO(13L, "tag13"),
                                                             new TagDTO(7L, "tag7"),
                                                             new TagDTO(8L, "tag8"),
                                                             new TagDTO(38L, "tag38"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_35
        = new GiftCertificateDTO(35L,
                                 "cert35",
                                 "description35",
                                 new BigDecimal("1908.00"),
                                 (short) 333,
                                 LocalDateTime.of(2022, 3, 2, 10, 23, 44, 0),
                                 LocalDateTime.of(2022, 3, 2, 14, 12, 1, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(17L, "tag17"),
                                                             new TagDTO(21L, "tag21"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_36
        = new GiftCertificateDTO(36L,
                                 "cert36",
                                 "description36",
                                 new BigDecimal("2748.00"),
                                 (short) 381,
                                 LocalDateTime.of(2022, 5, 27, 5, 56, 41, 0),
                                 LocalDateTime.of(2022, 5, 27, 7, 29, 13, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(50L, "tag50"),
                                                             new TagDTO(22L, "tag22"),
                                                             new TagDTO(8L, "tag8"),
                                                             new TagDTO(19L, "tag19"),
                                                             new TagDTO(42L, "tag42"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_37
        = new GiftCertificateDTO(37L,
                                 "cert37",
                                 "description37",
                                 new BigDecimal("4061.00"),
                                 (short) 595,
                                 LocalDateTime.of(2022, 3, 30, 18, 57, 52, 0),
                                 LocalDateTime.of(2022, 3, 30, 8, 23, 53, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(3L, "tag3"),
                                                             new TagDTO(16L, "tag16"),
                                                             new TagDTO(5L, "tag5"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_38
        = new GiftCertificateDTO(38L,
                                 "cert38",
                                 "description38",
                                 new BigDecimal("1948.00"),
                                 (short) 89,
                                 LocalDateTime.of(2022, 2, 22, 20, 28, 0, 0),
                                 LocalDateTime.of(2022, 2, 22, 9, 9, 5, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(22L, "tag22"), new TagDTO(25L, "tag25"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_39
        = new GiftCertificateDTO(39L,
                                 "cert39",
                                 "description39",
                                 new BigDecimal("2900.00"),
                                 (short) 396,
                                 LocalDateTime.of(2022, 2, 15, 22, 11, 48, 0),
                                 LocalDateTime.of(2022, 2, 15, 9, 40, 11, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(2L, "tag2"),
                                                             new TagDTO(26L, "tag26"),
                                                             new TagDTO(15L, "tag15"),
                                                             new TagDTO(42L, "tag42"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_40
        = new GiftCertificateDTO(40L,
                                 "cert40",
                                 "description40",
                                 new BigDecimal("2124.00"),
                                 (short) 452,
                                 LocalDateTime.of(2022, 4, 2, 19, 33, 36, 0),
                                 LocalDateTime.of(2022, 4, 2, 2, 28, 31, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(32L, "tag32"),
                                                             new TagDTO(16L, "tag16"),
                                                             new TagDTO(29L, "tag29"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_41
        = new GiftCertificateDTO(41L,
                                 "cert41",
                                 "description41",
                                 new BigDecimal("2846.00"),
                                 (short) 237,
                                 LocalDateTime.of(2022, 4, 28, 19, 35, 41, 0),
                                 LocalDateTime.of(2022, 4, 28, 1, 22, 26, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(50L, "tag50"),
                                                             new TagDTO(21L, "tag21"),
                                                             new TagDTO(38L, "tag38"),
                                                             new TagDTO(11L, "tag11"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_42
        = new GiftCertificateDTO(42L,
                                 "cert42",
                                 "description42",
                                 new BigDecimal("3382.00"),
                                 (short) 533,
                                 LocalDateTime.of(2022, 3, 24, 3, 30, 6, 0),
                                 LocalDateTime.of(2022, 3, 24, 5, 51, 19, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(35L, "tag35"), new TagDTO(24L, "tag24"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_43
        = new GiftCertificateDTO(43L,
                                 "cert43",
                                 "description43",
                                 new BigDecimal("3843.00"),
                                 (short) 299,
                                 LocalDateTime.of(2022, 4, 4, 17, 57, 35, 0),
                                 LocalDateTime.of(2022, 4, 4, 22, 2, 26, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(18L, "tag18"),
                                                             new TagDTO(29L, "tag29"),
                                                             new TagDTO(47L, "tag47"),
                                                             new TagDTO(15L, "tag15"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_44
        = new GiftCertificateDTO(44L,
                                 "cert44",
                                 "description44",
                                 new BigDecimal("905.00"),
                                 (short) 314,
                                 LocalDateTime.of(2022, 3, 5, 1, 43, 4, 0),
                                 LocalDateTime.of(2022, 3, 5, 17, 59, 43, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(30L, "tag30"), new TagDTO(11L, "tag11"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_45
        = new GiftCertificateDTO(45L,
                                 "cert45",
                                 "description45",
                                 new BigDecimal("1230.00"),
                                 (short) 110,
                                 LocalDateTime.of(2022, 3, 15, 23, 50, 8, 0),
                                 LocalDateTime.of(2022, 3, 15, 19, 50, 12, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(49L, "tag49"),
                                                             new TagDTO(50L, "tag50"),
                                                             new TagDTO(3L, "tag3"),
                                                             new TagDTO(8L, "tag8"),
                                                             new TagDTO(38L, "tag38"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_46
        = new GiftCertificateDTO(46L,
                                 "cert46",
                                 "description46",
                                 new BigDecimal("1227.00"),
                                 (short) 318,
                                 LocalDateTime.of(2022, 3, 4, 4, 8, 57, 0),
                                 LocalDateTime.of(2022, 3, 4, 21, 53, 13, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(14L, "tag14"),
                                                             new TagDTO(36L, "tag36"),
                                                             new TagDTO(1L, "tag1"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_47
        = new GiftCertificateDTO(47L,
                                 "cert47",
                                 "description47",
                                 new BigDecimal("897.00"),
                                 (short) 119,
                                 LocalDateTime.of(2022, 3, 24, 15, 49, 11, 0),
                                 LocalDateTime.of(2022, 3, 24, 13, 32, 10, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(21L, "tag21"), new TagDTO(1L, "tag1"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_48
        = new GiftCertificateDTO(48L,
                                 "cert48",
                                 "description48",
                                 new BigDecimal("2609.00"),
                                 (short) 49,
                                 LocalDateTime.of(2022, 4, 15, 20, 10, 35, 0),
                                 LocalDateTime.of(2022, 4, 15, 6, 58, 25, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(2L, "tag2"),
                                                             new TagDTO(39L, "tag39"),
                                                             new TagDTO(25L, "tag25"),
                                                             new TagDTO(34L, "tag34"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_49
        = new GiftCertificateDTO(49L,
                                 "cert49",
                                 "description49",
                                 new BigDecimal("1323.00"),
                                 (short) 490,
                                 LocalDateTime.of(2022, 4, 25, 4, 20, 37, 0),
                                 LocalDateTime.of(2022, 4, 25, 3, 11, 59, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(50L, "tag50"),
                                                             new TagDTO(3L, "tag3"),
                                                             new TagDTO(38L, "tag38"),
                                                             new TagDTO(47L, "tag47"))));
    public static final GiftCertificateDTO GIFT_CERT_DTO_50
        = new GiftCertificateDTO(50L,
                                 "cert50",
                                 "description50",
                                 new BigDecimal("2423.00"),
                                 (short) 118,
                                 LocalDateTime.of(2022, 4, 21, 3, 16, 17, 0),
                                 LocalDateTime.of(2022, 4, 21, 16, 14, 48, 0),
                                 new HashSet<>(Arrays.asList(new TagDTO(32L, "tag32"),
                                                             new TagDTO(41L, "tag41"),
                                                             new TagDTO(49L, "tag49"),
                                                             new TagDTO(39L, "tag39"),
                                                             new TagDTO(47L, "tag47"))));
    public static final List<GiftCertificateDTO> GIFT_CERT_10_12_17_DTO_LIST
        = List.of(GIFT_CERT_DTO_10,
                  GIFT_CERT_DTO_12,
                  GIFT_CERT_DTO_17);
    public static final List<GiftCertificateDTO> GIFT_CERT_6_TO_10_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_6,
                        GIFT_CERT_DTO_7,
                        GIFT_CERT_DTO_8,
                        GIFT_CERT_DTO_9,
                        GIFT_CERT_DTO_10);
    public static final List<GiftCertificateDTO> GIFT_CERT_46_TO_50_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_46,
                        GIFT_CERT_DTO_47,
                        GIFT_CERT_DTO_48,
                        GIFT_CERT_DTO_49,
                        GIFT_CERT_DTO_50);
    public static final List<GiftCertificateDTO> GIFT_CERT_41_TO_45_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_41,
                        GIFT_CERT_DTO_42,
                        GIFT_CERT_DTO_43,
                        GIFT_CERT_DTO_44,
                        GIFT_CERT_DTO_45);
    public static final List<GiftCertificateDTO> GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_14,
                        GIFT_CERT_DTO_16,
                        GIFT_CERT_DTO_17,
                        GIFT_CERT_DTO_20,
                        GIFT_CERT_DTO_24,
                        GIFT_CERT_DTO_46,
                        GIFT_CERT_DTO_47);
    public static final List<GiftCertificateDTO> GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_3,
                        GIFT_CERT_DTO_13,
                        GIFT_CERT_DTO_23,
                        GIFT_CERT_DTO_30,
                        GIFT_CERT_DTO_31,
                        GIFT_CERT_DTO_32,
                        GIFT_CERT_DTO_33,
                        GIFT_CERT_DTO_34,
                        GIFT_CERT_DTO_35,
                        GIFT_CERT_DTO_36,
                        GIFT_CERT_DTO_37,
                        GIFT_CERT_DTO_38,
                        GIFT_CERT_DTO_39,
                        GIFT_CERT_DTO_43);
    public static final List<GiftCertificateDTO> GIFT_CERT_1_TO_5_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_1,
                        GIFT_CERT_DTO_2,
                        GIFT_CERT_DTO_3,
                        GIFT_CERT_DTO_4,
                        GIFT_CERT_DTO_5);
    public static final List<GiftCertificateDTO> GIFT_CERT_DTO_LIST_NAME_DESC_ORDER
        = Arrays.asList(GIFT_CERT_DTO_9,
                        GIFT_CERT_DTO_8,
                        GIFT_CERT_DTO_7,
                        GIFT_CERT_DTO_6,
                        GIFT_CERT_DTO_50);
    public static final List<GiftCertificateDTO> GIFT_CERT_DTO_LIST_DATE_ASC_ORDER
        = Arrays.asList(GIFT_CERT_DTO_4,
                        GIFT_CERT_DTO_8,
                        GIFT_CERT_DTO_18,
                        GIFT_CERT_DTO_13,
                        GIFT_CERT_DTO_39);
    public static final GiftCertificateDTO GIFT_CERT_DTO_PATCHED
        = new GiftCertificateDTO(1L,
                                 "cert1NamePatched",
                                 "cert1DescriptionPatched",
                                 new BigDecimal("500.00"),
                                 (short) 120,
                                 LocalDateTime.of(2022, 12, 30, 15, 7, 30, 965765000),
                                 LocalDateTime.of(2023, 1, 7, 18, 0, 0, 0),
                                 new HashSet<>(Set.of(new TagDTO(51L, "tag51"))));
    public static final List<GiftCertificateDTO> GIFT_CERT_1_TO_50_DTO_LIST
        = Arrays.asList(GIFT_CERT_DTO_1,
                        GIFT_CERT_DTO_2,
                        GIFT_CERT_DTO_3,
                        GIFT_CERT_DTO_4,
                        GIFT_CERT_DTO_5,
                        GIFT_CERT_DTO_6,
                        GIFT_CERT_DTO_7,
                        GIFT_CERT_DTO_8,
                        GIFT_CERT_DTO_9,
                        GIFT_CERT_DTO_10,
                        GIFT_CERT_DTO_11,
                        GIFT_CERT_DTO_12,
                        GIFT_CERT_DTO_13,
                        GIFT_CERT_DTO_14,
                        GIFT_CERT_DTO_15,
                        GIFT_CERT_DTO_16,
                        GIFT_CERT_DTO_17,
                        GIFT_CERT_DTO_18,
                        GIFT_CERT_DTO_19,
                        GIFT_CERT_DTO_20,
                        GIFT_CERT_DTO_21,
                        GIFT_CERT_DTO_22,
                        GIFT_CERT_DTO_23,
                        GIFT_CERT_DTO_24,
                        GIFT_CERT_DTO_25,
                        GIFT_CERT_DTO_26,
                        GIFT_CERT_DTO_27,
                        GIFT_CERT_DTO_28,
                        GIFT_CERT_DTO_29,
                        GIFT_CERT_DTO_30,
                        GIFT_CERT_DTO_31,
                        GIFT_CERT_DTO_32,
                        GIFT_CERT_DTO_33,
                        GIFT_CERT_DTO_34,
                        GIFT_CERT_DTO_35,
                        GIFT_CERT_DTO_36,
                        GIFT_CERT_DTO_37,
                        GIFT_CERT_DTO_38,
                        GIFT_CERT_DTO_39,
                        GIFT_CERT_DTO_40,
                        GIFT_CERT_DTO_41,
                        GIFT_CERT_DTO_42,
                        GIFT_CERT_DTO_43,
                        GIFT_CERT_DTO_44,
                        GIFT_CERT_DTO_45,
                        GIFT_CERT_DTO_46,
                        GIFT_CERT_DTO_47,
                        GIFT_CERT_DTO_48,
                        GIFT_CERT_DTO_49,
                        GIFT_CERT_DTO_50);

    private GiftCertificateDTOConstants() {
    }

    public static GiftCertificateDTO getGiftCertificateDTOToSave() {
        return new GiftCertificateDTO(null,
                                      "cert51Name",
                                      "cert51Description",
                                      new BigDecimal("50.00"),
                                      (short) 90,
                                      null,
                                      null,
                                      new HashSet<>(Arrays.asList(new TagDTO(null, "tag51"),
                                                                  new TagDTO(42L, "tag42"))));
    }

    public static GiftCertificateDTO getGiftCertificateDtoPatch() {
        return new GiftCertificateDTO(null,
                                      "cert1NamePatched",
                                      "cert1DescriptionPatched",
                                      new BigDecimal("500.00"),
                                      (short) 120,
                                      null,
                                      null,
                                      new HashSet<>(Set.of(new TagDTO(null, "tag51"))));
    }

}
