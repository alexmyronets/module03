package com.epam.esm.DTOConstants;

import com.epam.esm.dto.OrderDTO;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public abstract class OrderDTOConstants {

    public static final OrderDTO ORDER_DTO_1 = new OrderDTO(1L,
                                                            1L,
                                                            4L,
                                                            LocalDateTime.parse("2022-01-27T00:20:46.926409"),
                                                            new BigDecimal("220.50"));

    public static final OrderDTO ORDER_DTO_93 = new OrderDTO(93L,
                                                             17L,
                                                             16L,
                                                             LocalDateTime.parse("2022-04-14T15:43:54.000"),
                                                             new BigDecimal("2693.00"));

    public static final OrderDTO ORDER_DTO_94 = new OrderDTO(94L,
                                                             17L,
                                                             6L,
                                                             LocalDateTime.parse("2022-03-31T00:00:11.000"),
                                                             new BigDecimal("1163.00"));

    public static final OrderDTO ORDER_DTO_95 = new OrderDTO(95L,
                                                             17L,
                                                             2L,
                                                             LocalDateTime.parse("2023-01-27T15:25:05.523600"),
                                                             new BigDecimal("150.00"));

    public static final OrderDTO ORDER_DTO_96 = new OrderDTO(96L,
                                                             17L,
                                                             9L,
                                                             LocalDateTime.parse("2022-07-06T13:38:15.000"),
                                                             new BigDecimal("4056.00"));

    public static final OrderDTO ORDER_DTO_97 = new OrderDTO(97L,
                                                             17L,
                                                             46L,
                                                             LocalDateTime.parse("2022-03-09T04:08:57.000"),
                                                             new BigDecimal("1227.00"));

    public static final OrderDTO ORDER_DTO_98 = new OrderDTO(98L,
                                                             17L,
                                                             40L,
                                                             LocalDateTime.parse("2022-04-20T19:33:36.000"),
                                                             new BigDecimal("2124.00"));

    public static final OrderDTO ORDER_DTO_99 = new OrderDTO(99L,
                                                             17L,
                                                             2L,
                                                             LocalDateTime.parse("2023-02-08T15:25:05.523600"),
                                                             new BigDecimal("150.00"));

    public static final OrderDTO ORDER_DTO_100 = new OrderDTO(100L,
                                                              17L,
                                                              29L,
                                                              LocalDateTime.parse("2022-06-15T16:06:04.000"),
                                                              new BigDecimal("1899.00"));

    public static final OrderDTO ORDER_DTO_101 = new OrderDTO(101L,
                                                              17L,
                                                              2L,
                                                              LocalDateTime.parse("2023-01-03T15:25:05.523600"),
                                                              new BigDecimal("150.00"));

    public static final OrderDTO ORDER_DTO_102 = new OrderDTO(102L,
                                                              17L,
                                                              47L,
                                                              LocalDateTime.parse("2022-03-25T15:49:11.000"),
                                                              new BigDecimal("897.00"));

    public static final OrderDTO ORDER_DTO_103 = new OrderDTO(103L,
                                                              17L,
                                                              19L,
                                                              LocalDateTime.parse("2022-03-25T17:30:11.000"),
                                                              new BigDecimal("519.00"));

    public static final OrderDTO CREATED_ORDER_DTO = new OrderDTO(273L,
                                                                  1L,
                                                                  42L,
                                                                  LocalDateTime.parse("2023-03-08T20:34:54.826"),
                                                                  new BigDecimal("3382.00"));

    public static final List<OrderDTO> ORDER_98_TO_102_DTO_LIST
        = List.of(ORDER_DTO_98,
                  ORDER_DTO_99,
                  ORDER_DTO_100,
                  ORDER_DTO_101,
                  ORDER_DTO_102);

    public static final List<OrderDTO> ORDER_93_TO_97_DTO_LIST
        = List.of(ORDER_DTO_93,
                  ORDER_DTO_94,
                  ORDER_DTO_95,
                  ORDER_DTO_96,
                  ORDER_DTO_97);

    public static final List<OrderDTO> ORDER_99_TO_103_DTO_LIST
        = List.of(ORDER_DTO_99,
                  ORDER_DTO_100,
                  ORDER_DTO_101,
                  ORDER_DTO_102,
                  ORDER_DTO_103);

    public static final List<OrderDTO> ORDER_94_TO_98_DTO_LIST
        = List.of(ORDER_DTO_94,
                  ORDER_DTO_95,
                  ORDER_DTO_96,
                  ORDER_DTO_97,
                  ORDER_DTO_98);

    public static final List<OrderDTO> ORDER_93_TO_103_DTO_LIST
        = List.of(ORDER_DTO_93,
                  ORDER_DTO_94,
                  ORDER_DTO_95,
                  ORDER_DTO_96,
                  ORDER_DTO_97,
                  ORDER_DTO_98,
                  ORDER_DTO_99,
                  ORDER_DTO_100,
                  ORDER_DTO_101,
                  ORDER_DTO_102,
                  ORDER_DTO_103);

    private OrderDTOConstants() {
    }

    public static OrderDTO getOrderDTOToCreate() {
        return new OrderDTO(null,
                            1L,
                            42L,
                            null,
                            null);
    }
}
