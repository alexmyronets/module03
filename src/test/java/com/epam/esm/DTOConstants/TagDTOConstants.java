package com.epam.esm.DTOConstants;


import com.epam.esm.dto.TagDTO;
import java.util.Arrays;
import java.util.List;

public abstract class TagDTOConstants {

    public static final TagDTO TAG_DTO_TO_SAVE = new TagDTO(null, "tag51");
    public static final TagDTO SAVED_TAG_DTO = new TagDTO(51L, "tag51");
    public static final TagDTO TAG_DTO_EXISTS_TO_CREATE = new TagDTO(null, "tag5");

    public static final TagDTO TAG_DTO_1 = new TagDTO(1L, "tag1");
    public static final TagDTO TAG_DTO_2 = new TagDTO(2L, "tag2");
    public static final TagDTO TAG_DTO_3 = new TagDTO(3L, "tag3");
    public static final TagDTO TAG_DTO_4 = new TagDTO(4L, "tag4");
    public static final TagDTO TAG_DTO_5 = new TagDTO(5L, "tag5");
    public static final TagDTO TAG_DTO_6 = new TagDTO(6L, "tag6");
    public static final TagDTO TAG_DTO_7 = new TagDTO(7L, "tag7");
    public static final TagDTO TAG_DTO_8 = new TagDTO(8L, "tag8");
    public static final TagDTO TAG_DTO_9 = new TagDTO(9L, "tag9");
    public static final TagDTO TAG_DTO_10 = new TagDTO(10L, "tag10");
    public static final TagDTO TAG_DTO_11 = new TagDTO(11L, "tag11");
    public static final TagDTO TAG_DTO_12 = new TagDTO(12L, "tag12");
    public static final TagDTO TAG_DTO_13 = new TagDTO(13L, "tag13");
    public static final TagDTO TAG_DTO_14 = new TagDTO(14L, "tag14");
    public static final TagDTO TAG_DTO_15 = new TagDTO(15L, "tag15");
    public static final TagDTO TAG_DTO_16 = new TagDTO(16L, "tag16");
    public static final TagDTO TAG_DTO_17 = new TagDTO(17L, "tag17");
    public static final TagDTO TAG_DTO_18 = new TagDTO(18L, "tag18");
    public static final TagDTO TAG_DTO_19 = new TagDTO(19L, "tag19");
    public static final TagDTO TAG_DTO_20 = new TagDTO(20L, "tag20");
    public static final TagDTO TAG_DTO_21 = new TagDTO(21L, "tag21");
    public static final TagDTO TAG_DTO_22 = new TagDTO(22L, "tag22");
    public static final TagDTO TAG_DTO_23 = new TagDTO(23L, "tag23");
    public static final TagDTO TAG_DTO_24 = new TagDTO(24L, "tag24");
    public static final TagDTO TAG_DTO_25 = new TagDTO(25L, "tag25");
    public static final TagDTO TAG_DTO_26 = new TagDTO(26L, "tag26");
    public static final TagDTO TAG_DTO_27 = new TagDTO(27L, "tag27");
    public static final TagDTO TAG_DTO_28 = new TagDTO(28L, "tag28");
    public static final TagDTO TAG_DTO_29 = new TagDTO(29L, "tag29");
    public static final TagDTO TAG_DTO_30 = new TagDTO(30L, "tag30");
    public static final TagDTO TAG_DTO_31 = new TagDTO(31L, "tag31");
    public static final TagDTO TAG_DTO_32 = new TagDTO(32L, "tag32");
    public static final TagDTO TAG_DTO_33 = new TagDTO(33L, "tag33");
    public static final TagDTO TAG_DTO_34 = new TagDTO(34L, "tag34");
    public static final TagDTO TAG_DTO_35 = new TagDTO(35L, "tag35");
    public static final TagDTO TAG_DTO_36 = new TagDTO(36L, "tag36");
    public static final TagDTO TAG_DTO_37 = new TagDTO(37L, "tag37");
    public static final TagDTO TAG_DTO_38 = new TagDTO(38L, "tag38");
    public static final TagDTO TAG_DTO_39 = new TagDTO(39L, "tag39");
    public static final TagDTO TAG_DTO_40 = new TagDTO(40L, "tag40");
    public static final TagDTO TAG_DTO_41 = new TagDTO(41L, "tag41");
    public static final TagDTO TAG_DTO_42 = new TagDTO(42L, "tag42");
    public static final TagDTO TAG_DTO_43 = new TagDTO(43L, "tag43");
    public static final TagDTO TAG_DTO_44 = new TagDTO(44L, "tag44");
    public static final TagDTO TAG_DTO_45 = new TagDTO(45L, "tag45");
    public static final TagDTO TAG_DTO_46 = new TagDTO(46L, "tag46");
    public static final TagDTO TAG_DTO_47 = new TagDTO(47L, "tag47");
    public static final TagDTO TAG_DTO_48 = new TagDTO(48L, "tag48");
    public static final TagDTO TAG_DTO_49 = new TagDTO(49L, "tag49");
    public static final TagDTO TAG_DTO_50 = new TagDTO(50L, "tag50");

    public static final List<TagDTO> TAG_6_TO_10_DTO_LIST
        = List.of(TAG_DTO_6,
                  TAG_DTO_7,
                  TAG_DTO_8,
                  TAG_DTO_9,
                  TAG_DTO_10);

    public static final List<TagDTO> TAG_1_TO_5_DTO_LIST
        = List.of(TAG_DTO_1,
                  TAG_DTO_2,
                  TAG_DTO_3,
                  TAG_DTO_4,
                  TAG_DTO_5);

    public static final List<TagDTO> TAG_46_TO_50_DTO_LIST
        = List.of(TAG_DTO_46,
                  TAG_DTO_47,
                  TAG_DTO_48,
                  TAG_DTO_49,
                  TAG_DTO_50);

    public static final List<TagDTO> TAG_41_TO_55_DTO_LIST
        = List.of(TAG_DTO_41,
                  TAG_DTO_42,
                  TAG_DTO_43,
                  TAG_DTO_44,
                  TAG_DTO_45);

    public static final List<TagDTO> TAG_1_TO_50_DTO_LIST
        = List.of(TAG_DTO_1,
                  TAG_DTO_2,
                  TAG_DTO_3,
                  TAG_DTO_4,
                  TAG_DTO_5,
                  TAG_DTO_6,
                  TAG_DTO_7,
                  TAG_DTO_8,
                  TAG_DTO_9,
                  TAG_DTO_10,
                  TAG_DTO_11,
                  TAG_DTO_12,
                  TAG_DTO_13,
                  TAG_DTO_14,
                  TAG_DTO_15,
                  TAG_DTO_16,
                  TAG_DTO_17,
                  TAG_DTO_18,
                  TAG_DTO_19,
                  TAG_DTO_20,
                  TAG_DTO_21,
                  TAG_DTO_22,
                  TAG_DTO_23,
                  TAG_DTO_24,
                  TAG_DTO_25,
                  TAG_DTO_26,
                  TAG_DTO_27,
                  TAG_DTO_28,
                  TAG_DTO_29,
                  TAG_DTO_30,
                  TAG_DTO_31,
                  TAG_DTO_32,
                  TAG_DTO_33,
                  TAG_DTO_34,
                  TAG_DTO_35,
                  TAG_DTO_36,
                  TAG_DTO_37,
                  TAG_DTO_38,
                  TAG_DTO_39,
                  TAG_DTO_40,
                  TAG_DTO_41,
                  TAG_DTO_42,
                  TAG_DTO_43,
                  TAG_DTO_44,
                  TAG_DTO_45,
                  TAG_DTO_46,
                  TAG_DTO_47,
                  TAG_DTO_48,
                  TAG_DTO_49,
                  TAG_DTO_50);

    public static final List<TagDTO> TAG_DTO_LIST
        = Arrays.asList(TAG_DTO_1,
                        new TagDTO(2L, "tag2"),
                        new TagDTO(3L, "tag3"),
                        new TagDTO(4L, "tag4"),
                        new TagDTO(5L, "tag5"),
                        new TagDTO(6L, "tag6"),
                        new TagDTO(7L, "tag7"),
                        new TagDTO(8L, "tag8"),
                        new TagDTO(9L, "tag9"),
                        new TagDTO(10L, "tag10"),
                        new TagDTO(11L, "tag11"));

    private TagDTOConstants() {
    }
}
