package com.epam.esm.DTOConstants;

import com.epam.esm.dto.paging.PagingParamsDTO;

public abstract class PagingParamsDTOConstants {

    public static final PagingParamsDTO EMPTY_PAGING = new PagingParamsDTO(-1L, -1L, -1L);
    public static final PagingParamsDTO NEXT_PAGING = new PagingParamsDTO(10L, 6L, 45L);
    public static final PagingParamsDTO FIRST_PAGING = new PagingParamsDTO(5L, -1L, 45L);
    public static final PagingParamsDTO LAST_PAGING = new PagingParamsDTO(-1L, 46L, 45L);
    public static final PagingParamsDTO PREV_PAGING = new PagingParamsDTO(45L, 41L, 45L);

    public static final PagingParamsDTO NAME_DESC_ORDER_PAGING = new PagingParamsDTO(50L, -1L, 14L);
    public static final PagingParamsDTO DATE_ASC_ORDER_PAGING = new PagingParamsDTO(39L, -1L, 9L);

    public static final PagingParamsDTO ORDER_NEXT_PAGING = new PagingParamsDTO(102L, 98L, 98L);
    public static final PagingParamsDTO ORDER_FIRST_PAGING = new PagingParamsDTO(97L, -1L, 98L);
    public static final PagingParamsDTO ORDER_LAST_PAGING = new PagingParamsDTO(-1L, 99L, 98L);
    public static final PagingParamsDTO ORDER_PREV_PAGING = new PagingParamsDTO(98L, 94L, 98L);

    private PagingParamsDTOConstants() {
    }
}
