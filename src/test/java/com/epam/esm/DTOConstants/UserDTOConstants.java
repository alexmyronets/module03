package com.epam.esm.DTOConstants;

import com.epam.esm.dto.UserDTO;
import java.util.List;

public abstract class UserDTOConstants {

    public static final UserDTO USER_DTO_1 = new UserDTO(1L, "user1");
    public static final UserDTO USER_DTO_2 = new UserDTO(2L, "user2");
    public static final UserDTO USER_DTO_3 = new UserDTO(3L, "user3");
    public static final UserDTO USER_DTO_4 = new UserDTO(4L, "user4");
    public static final UserDTO USER_DTO_5 = new UserDTO(5L, "user5");
    public static final UserDTO USER_DTO_6 = new UserDTO(6L, "user6");
    public static final UserDTO USER_DTO_7 = new UserDTO(7L, "user7");
    public static final UserDTO USER_DTO_8 = new UserDTO(8L, "user8");
    public static final UserDTO USER_DTO_9 = new UserDTO(9L, "user9");
    public static final UserDTO USER_DTO_10 = new UserDTO(10L, "user10");
    public static final UserDTO USER_DTO_11 = new UserDTO(11L, "user11");
    public static final UserDTO USER_DTO_12 = new UserDTO(12L, "user12");
    public static final UserDTO USER_DTO_13 = new UserDTO(13L, "user13");
    public static final UserDTO USER_DTO_14 = new UserDTO(14L, "user14");
    public static final UserDTO USER_DTO_15 = new UserDTO(15L, "user15");
    public static final UserDTO USER_DTO_16 = new UserDTO(16L, "user16");
    public static final UserDTO USER_DTO_17 = new UserDTO(17L, "user17");
    public static final UserDTO USER_DTO_18 = new UserDTO(18L, "user18");
    public static final UserDTO USER_DTO_19 = new UserDTO(19L, "user19");
    public static final UserDTO USER_DTO_20 = new UserDTO(20L, "user20");
    public static final UserDTO USER_DTO_21 = new UserDTO(21L, "user21");
    public static final UserDTO USER_DTO_22 = new UserDTO(22L, "user22");
    public static final UserDTO USER_DTO_23 = new UserDTO(23L, "user23");
    public static final UserDTO USER_DTO_24 = new UserDTO(24L, "user24");
    public static final UserDTO USER_DTO_25 = new UserDTO(25L, "user25");
    public static final UserDTO USER_DTO_26 = new UserDTO(26L, "user26");
    public static final UserDTO USER_DTO_27 = new UserDTO(27L, "user27");
    public static final UserDTO USER_DTO_28 = new UserDTO(28L, "user28");
    public static final UserDTO USER_DTO_29 = new UserDTO(29L, "user29");
    public static final UserDTO USER_DTO_30 = new UserDTO(30L, "user30");
    public static final UserDTO USER_DTO_31 = new UserDTO(31L, "user31");
    public static final UserDTO USER_DTO_32 = new UserDTO(32L, "user32");
    public static final UserDTO USER_DTO_33 = new UserDTO(33L, "user33");
    public static final UserDTO USER_DTO_34 = new UserDTO(34L, "user34");
    public static final UserDTO USER_DTO_35 = new UserDTO(35L, "user35");
    public static final UserDTO USER_DTO_36 = new UserDTO(36L, "user36");
    public static final UserDTO USER_DTO_37 = new UserDTO(37L, "user37");
    public static final UserDTO USER_DTO_38 = new UserDTO(38L, "user38");
    public static final UserDTO USER_DTO_39 = new UserDTO(39L, "user39");
    public static final UserDTO USER_DTO_40 = new UserDTO(40L, "user40");
    public static final UserDTO USER_DTO_41 = new UserDTO(41L, "user41");
    public static final UserDTO USER_DTO_42 = new UserDTO(42L, "user42");
    public static final UserDTO USER_DTO_43 = new UserDTO(43L, "user43");
    public static final UserDTO USER_DTO_44 = new UserDTO(44L, "user44");
    public static final UserDTO USER_DTO_45 = new UserDTO(45L, "user45");
    public static final UserDTO USER_DTO_46 = new UserDTO(46L, "user46");
    public static final UserDTO USER_DTO_47 = new UserDTO(47L, "user47");
    public static final UserDTO USER_DTO_48 = new UserDTO(48L, "user48");
    public static final UserDTO USER_DTO_49 = new UserDTO(49L, "user49");
    public static final UserDTO USER_DTO_50 = new UserDTO(50L, "user50");

    public static List<UserDTO> USER_6_TO_10_DTO_LIST
        = List.of(USER_DTO_6,
                  USER_DTO_7,
                  USER_DTO_8,
                  USER_DTO_9,
                  USER_DTO_10);

    public static List<UserDTO> USER_1_TO_5_DTO_LIST
        = List.of(USER_DTO_1,
                  USER_DTO_2,
                  USER_DTO_3,
                  USER_DTO_4,
                  USER_DTO_5);

    public static List<UserDTO> USER_46_TO_50_DTO_LIST
        = List.of(USER_DTO_46,
                  USER_DTO_47,
                  USER_DTO_48,
                  USER_DTO_49,
                  USER_DTO_50);

    public static List<UserDTO> USER_41_TO_45_DTO_LIST
        = List.of(USER_DTO_41,
                  USER_DTO_42,
                  USER_DTO_43,
                  USER_DTO_44,
                  USER_DTO_45);

    public static List<UserDTO> USER_1_TO_50_DTO_LIST
        = List.of(USER_DTO_1,
                  USER_DTO_2,
                  USER_DTO_3,
                  USER_DTO_4,
                  USER_DTO_5,
                  USER_DTO_6,
                  USER_DTO_7,
                  USER_DTO_8,
                  USER_DTO_9,
                  USER_DTO_10,
                  USER_DTO_11,
                  USER_DTO_12,
                  USER_DTO_13,
                  USER_DTO_14,
                  USER_DTO_15,
                  USER_DTO_16,
                  USER_DTO_17,
                  USER_DTO_18,
                  USER_DTO_19,
                  USER_DTO_20,
                  USER_DTO_21,
                  USER_DTO_22,
                  USER_DTO_23,
                  USER_DTO_24,
                  USER_DTO_25,
                  USER_DTO_26,
                  USER_DTO_27,
                  USER_DTO_28,
                  USER_DTO_29,
                  USER_DTO_30,
                  USER_DTO_31,
                  USER_DTO_32,
                  USER_DTO_33,
                  USER_DTO_34,
                  USER_DTO_35,
                  USER_DTO_36,
                  USER_DTO_37,
                  USER_DTO_38,
                  USER_DTO_39,
                  USER_DTO_40,
                  USER_DTO_41,
                  USER_DTO_42,
                  USER_DTO_43,
                  USER_DTO_44,
                  USER_DTO_45,
                  USER_DTO_46,
                  USER_DTO_47,
                  USER_DTO_48,
                  USER_DTO_49,
                  USER_DTO_50);

    private UserDTOConstants() {
    }
}
