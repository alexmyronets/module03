package com.epam.esm.web;

import static com.epam.esm.TestUtils.getBaseUrl;
import static com.epam.esm.TestUtils.getJsonString;
import static com.epam.esm.TestUtils.getUpdateBaseURL;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    private static final String JSON_PATH = "json/user/";

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResult")
    void getUsersList_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingUser_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingUserResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/users/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingUser_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingUserResponse.json", getClass());

        mockMvc.perform(get("/api/users/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    private static Stream<Arguments> provideUserPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/users?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      "[GET]NextPageResponse.json"),
                         Arguments.of("/api/users?page.size=5", "[GET]FirstPageResponse.json"),
                         Arguments.of("/api/users?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      "[GET]LastPageResponse.json"),
                         Arguments.of("/api/users?page.size=5&page.afterId=46&page.direction=BACK",
                                      "[GET]PreviousPageResponse.json"),
                         Arguments.of("/api/users", "[GET]NoPageParamsResponse.json"));
    }
}
