package com.epam.esm.web;

import static com.epam.esm.DateConstants.ISO_8601_DATE_REGEX;
import static com.epam.esm.TestUtils.getBaseUrl;
import static com.epam.esm.TestUtils.getJsonString;
import static com.epam.esm.TestUtils.getUpdateBaseURL;
import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerIntegrationTest {

    private static final String JSON_PATH = "json/order/";

    @Autowired
    MockMvc mockMvc;

    @ParameterizedTest
    @MethodSource("provideOrdersPageParamsAndResult")
    void getOrdersListByUser_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingOrderByUser_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingOrderResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/users/1/orders/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingOrderByUser_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingOrderResponse.json", getClass());

        mockMvc.perform(get("/api/users/1/orders/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @DirtiesContext
    void postValidOrder_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidOrderRequest.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);

        mockMvc.perform(post("/api/users/1/orders").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(jsonPath("$.certificateId").value("42"))
               .andExpect(jsonPath("$.orderDate", matchesPattern(ISO_8601_DATE_REGEX)))
               .andExpect(jsonPath("$.orderCost").value("3382.0"))
               .andExpect(jsonPath("$._links.self.href").value(baseUrl + "/api/users/1/orders/273"));
    }

    private static Stream<Arguments> provideOrdersPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/users/17/orders?page.size=5&page.afterId=97&page.direction=FORWARD",
                                      "[GET]UserOrdersNextPageResponse.json"),
                         Arguments.of("/api/users/17/orders?page.size=5", "[GET]UserOrdersFirstPageResponse.json"),
                         Arguments.of("/api/users/17/orders?page.size=5&page.afterId=98&page.direction=FORWARD",
                                      "[GET]UserOrdersLastPageResponse.json"),
                         Arguments.of("/api/users/17/orders?page.size=5&page.afterId=99&page.direction=BACK",
                                      "[GET]UserOrdersPreviousPageResponse.json"),
                         Arguments.of("/api/users/17/orders", "[GET]UserOrdersNoPageParamsResponse.json"));
    }
}
