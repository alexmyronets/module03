package com.epam.esm.web;

import static com.epam.esm.TestUtils.getBaseUrl;
import static com.epam.esm.TestUtils.getJsonString;
import static com.epam.esm.TestUtils.getUpdateBaseURL;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class TagControllerIntegrationTest {

    private static final String JSON_PATH = "json/tag/";

    @Autowired
    MockMvc mockMvc;

    @Test
    @DirtiesContext
    void postValidTag_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(post("/api/tags").content(requestBodyJson)
                                         .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postExistingTag_thenConflict() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingTagResponse.json", getClass());

        mockMvc.perform(post("/api/tags").content(requestBodyJson)
                                         .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResult")
    void getTagsList_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingTag_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingTagResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/tags/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingTagResponse.json", getClass());

        mockMvc.perform(get("/api/tags/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @DirtiesContext
    void deleteNotUsedTag_thenNoContent() throws Exception {
        mockMvc.perform(delete("/api/tags/9"))
               .andExpect(status().isNoContent());
    }

    @Test
    void deleteUsedTag_thenConflict() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]UsedTagResponse.json", getClass());

        mockMvc.perform(delete("/api/tags/5"))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]NonExistingTagResponse.json", getClass());

        mockMvc.perform(delete("/api/tags/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getMostUsedTagByMostSpentUserOrders_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]MostUsedTagByMostSpentUserOrders.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/tags/most_used_tag_by_most_spent_user"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE)).andExpect(content().json(responseBodyJson));

    }

    private static Stream<Arguments> provideTagPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/tags?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      "[GET]NextPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5", "[GET]FirstPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      "[GET]LastPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5&page.afterId=46&page.direction=BACK",
                                      "[GET]PreviousPageResponse.json"),
                         Arguments.of("/api/tags", "[GET]NoPageParamsResponse.json"));
    }

}