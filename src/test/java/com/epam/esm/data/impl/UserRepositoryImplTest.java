package com.epam.esm.data.impl;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_1_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_1_TO_5_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_41_TO_45_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_46_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_6_TO_10_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_ENTITY_1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchUserException;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@DataJpaTest
class UserRepositoryImplTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void readExistingUser() {
        assertEquals(USER_ENTITY_1, userRepository.read(1));
    }

    @Test
    void readNonExistingUser() {
        assertThrows(NoSuchUserException.class, () -> userRepository.read(442L));
    }

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResult")
    void list(FilterParams filterParams, List<User> userList) {
        assertEquals(userList, userRepository.list(filterParams));
    }

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResultAndPaging")
    void getNextAfterId(FilterParams filterParams, List<User> userList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getNextAfterId(),
                     userRepository.getNextAfterId(filterParams, userList.get(userList.size() - 1).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResultAndPaging")
    void getPrevAfterId(FilterParams filterParams, List<User> userList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getPrevAfterId(),
                     userRepository.getPrevAfterId(filterParams, userList.get(0).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResultAndPaging")
    void getLastAfterId(FilterParams filterParams, List<User> userList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getLastAfterId(),
                     userRepository.getLastAfterId(filterParams));
    }

    private static Stream<Arguments> provideUserPageParamsAndResult() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS, USER_6_TO_10_ENTITY_LIST),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS, USER_1_TO_5_ENTITY_LIST),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS, USER_46_TO_50_ENTITY_LIST),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS, USER_41_TO_45_ENTITY_LIST),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS, USER_1_TO_50_ENTITY_LIST));
    }

    private static Stream<Arguments> provideUserPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      USER_6_TO_10_ENTITY_LIST,
                                      NEXT_PAGING),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      USER_1_TO_5_ENTITY_LIST,
                                      FIRST_PAGING),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      USER_46_TO_50_ENTITY_LIST,
                                      LAST_PAGING),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      USER_41_TO_45_ENTITY_LIST,
                                      PREV_PAGING),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      USER_1_TO_50_ENTITY_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class UserRepositoryTestContextConfiguration {

        @Bean
        public UserRepository userRepo(EntityManager entityManager) {
            return new UserRepositoryImpl(entityManager);
        }

    }
}