package com.epam.esm.EntityConstants;

import com.epam.esm.domain.Tag;
import java.util.List;

public abstract class TagEntityConstants {

    public static final Tag SAVED_TAG = new Tag(51L, "tag51");
    public static final Tag TAG_ENTITY_EXISTS_TO_CREATE = new Tag(null, "tag5");
    public static final Tag TAG_ENTITY_1 = new Tag(1L, "tag1");
    public static final Tag TAG_ENTITY_2 = new Tag(2L, "tag2");
    public static final Tag TAG_ENTITY_3 = new Tag(3L, "tag3");
    public static final Tag TAG_ENTITY_4 = new Tag(4L, "tag4");
    public static final Tag TAG_ENTITY_5 = new Tag(5L, "tag5");
    public static final Tag TAG_ENTITY_6 = new Tag(6L, "tag6");
    public static final Tag TAG_ENTITY_7 = new Tag(7L, "tag7");
    public static final Tag TAG_ENTITY_8 = new Tag(8L, "tag8");
    public static final Tag TAG_ENTITY_9 = new Tag(9L, "tag9");
    public static final Tag TAG_ENTITY_10 = new Tag(10L, "tag10");
    public static final Tag TAG_ENTITY_11 = new Tag(11L, "tag11");
    public static final Tag TAG_ENTITY_12 = new Tag(12L, "tag12");
    public static final Tag TAG_ENTITY_13 = new Tag(13L, "tag13");
    public static final Tag TAG_ENTITY_14 = new Tag(14L, "tag14");
    public static final Tag TAG_ENTITY_15 = new Tag(15L, "tag15");
    public static final Tag TAG_ENTITY_16 = new Tag(16L, "tag16");
    public static final Tag TAG_ENTITY_17 = new Tag(17L, "tag17");
    public static final Tag TAG_ENTITY_18 = new Tag(18L, "tag18");
    public static final Tag TAG_ENTITY_19 = new Tag(19L, "tag19");
    public static final Tag TAG_ENTITY_20 = new Tag(20L, "tag20");
    public static final Tag TAG_ENTITY_21 = new Tag(21L, "tag21");
    public static final Tag TAG_ENTITY_22 = new Tag(22L, "tag22");
    public static final Tag TAG_ENTITY_23 = new Tag(23L, "tag23");
    public static final Tag TAG_ENTITY_24 = new Tag(24L, "tag24");
    public static final Tag TAG_ENTITY_25 = new Tag(25L, "tag25");
    public static final Tag TAG_ENTITY_26 = new Tag(26L, "tag26");
    public static final Tag TAG_ENTITY_27 = new Tag(27L, "tag27");
    public static final Tag TAG_ENTITY_28 = new Tag(28L, "tag28");
    public static final Tag TAG_ENTITY_29 = new Tag(29L, "tag29");
    public static final Tag TAG_ENTITY_30 = new Tag(30L, "tag30");
    public static final Tag TAG_ENTITY_31 = new Tag(31L, "tag31");
    public static final Tag TAG_ENTITY_32 = new Tag(32L, "tag32");
    public static final Tag TAG_ENTITY_33 = new Tag(33L, "tag33");
    public static final Tag TAG_ENTITY_34 = new Tag(34L, "tag34");
    public static final Tag TAG_ENTITY_35 = new Tag(35L, "tag35");
    public static final Tag TAG_ENTITY_36 = new Tag(36L, "tag36");
    public static final Tag TAG_ENTITY_37 = new Tag(37L, "tag37");
    public static final Tag TAG_ENTITY_38 = new Tag(38L, "tag38");
    public static final Tag TAG_ENTITY_39 = new Tag(39L, "tag39");
    public static final Tag TAG_ENTITY_40 = new Tag(40L, "tag40");
    public static final Tag TAG_ENTITY_41 = new Tag(41L, "tag41");
    public static final Tag TAG_ENTITY_42 = new Tag(42L, "tag42");
    public static final Tag TAG_ENTITY_43 = new Tag(43L, "tag43");
    public static final Tag TAG_ENTITY_44 = new Tag(44L, "tag44");
    public static final Tag TAG_ENTITY_45 = new Tag(45L, "tag45");
    public static final Tag TAG_ENTITY_46 = new Tag(46L, "tag46");
    public static final Tag TAG_ENTITY_47 = new Tag(47L, "tag47");
    public static final Tag TAG_ENTITY_48 = new Tag(48L, "tag48");
    public static final Tag TAG_ENTITY_49 = new Tag(49L, "tag49");
    public static final Tag TAG_ENTITY_50 = new Tag(50L, "tag50");

    public static final List<Tag> TAG_6_TO_10_ENTITY_LIST
        = List.of(TAG_ENTITY_6,
                  TAG_ENTITY_7,
                  TAG_ENTITY_8,
                  TAG_ENTITY_9,
                  TAG_ENTITY_10);

    public static final List<Tag> TAG_1_TO_5_ENTITY_LIST
        = List.of(TAG_ENTITY_1,
                  TAG_ENTITY_2,
                  TAG_ENTITY_3,
                  TAG_ENTITY_4,
                  TAG_ENTITY_5);

    public static final List<Tag> TAG_46_TO_50_ENTITY_LIST
        = List.of(TAG_ENTITY_46,
                  TAG_ENTITY_47,
                  TAG_ENTITY_48,
                  TAG_ENTITY_49,
                  TAG_ENTITY_50);

    public static final List<Tag> TAG_41_TO_55_ENTITY_LIST
        = List.of(TAG_ENTITY_41,
                  TAG_ENTITY_42,
                  TAG_ENTITY_43,
                  TAG_ENTITY_44,
                  TAG_ENTITY_45);

    public static final List<Tag> TAG_1_TO_50_ENTITY_LIST
        = List.of(TAG_ENTITY_1,
                  TAG_ENTITY_2,
                  TAG_ENTITY_3,
                  TAG_ENTITY_4,
                  TAG_ENTITY_5,
                  TAG_ENTITY_6,
                  TAG_ENTITY_7,
                  TAG_ENTITY_8,
                  TAG_ENTITY_9,
                  TAG_ENTITY_10,
                  TAG_ENTITY_11,
                  TAG_ENTITY_12,
                  TAG_ENTITY_13,
                  TAG_ENTITY_14,
                  TAG_ENTITY_15,
                  TAG_ENTITY_16,
                  TAG_ENTITY_17,
                  TAG_ENTITY_18,
                  TAG_ENTITY_19,
                  TAG_ENTITY_20,
                  TAG_ENTITY_21,
                  TAG_ENTITY_22,
                  TAG_ENTITY_23,
                  TAG_ENTITY_24,
                  TAG_ENTITY_25,
                  TAG_ENTITY_26,
                  TAG_ENTITY_27,
                  TAG_ENTITY_28,
                  TAG_ENTITY_29,
                  TAG_ENTITY_30,
                  TAG_ENTITY_31,
                  TAG_ENTITY_32,
                  TAG_ENTITY_33,
                  TAG_ENTITY_34,
                  TAG_ENTITY_35,
                  TAG_ENTITY_36,
                  TAG_ENTITY_37,
                  TAG_ENTITY_38,
                  TAG_ENTITY_39,
                  TAG_ENTITY_40,
                  TAG_ENTITY_41,
                  TAG_ENTITY_42,
                  TAG_ENTITY_43,
                  TAG_ENTITY_44,
                  TAG_ENTITY_45,
                  TAG_ENTITY_46,
                  TAG_ENTITY_47,
                  TAG_ENTITY_48,
                  TAG_ENTITY_49,
                  TAG_ENTITY_50);

    private TagEntityConstants() {
    }

    public static Tag getTagEntityToSave() {
        return new Tag(null, "tag51");
    }
}
