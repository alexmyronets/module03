package com.epam.esm.EntityConstants;


import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_16;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_19;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_2;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_29;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_4;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_40;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_42;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_46;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_47;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_6;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_9;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_ENTITY_1;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_ENTITY_17;

import com.epam.esm.domain.Order;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public abstract class OrderEntityConstants {

    public static final Order ORDER_ENTITY_1 = new Order(1L,
                                                         USER_ENTITY_1,
                                                         GIFT_CERT_ENTITY_4,
                                                         LocalDateTime.parse("2022-01-27T00:20:46.926409"),
                                                         new BigDecimal("220.50"));

    public static final Order ORDER_ENTITY_93 = new Order(93L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_16,
                                                          LocalDateTime.parse("2022-04-14T15:43:54.000"),
                                                          new BigDecimal("2693.00"));

    public static final Order ORDER_ENTITY_94 = new Order(94L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_6,
                                                          LocalDateTime.parse("2022-03-31T00:00:11.000"),
                                                          new BigDecimal("1163.00"));

    public static final Order ORDER_ENTITY_95 = new Order(95L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_2,
                                                          LocalDateTime.parse("2023-01-27T15:25:05.523600"),
                                                          new BigDecimal("150.00"));

    public static final Order ORDER_ENTITY_96 = new Order(96L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_9,
                                                          LocalDateTime.parse("2022-07-06T13:38:15.000"),
                                                          new BigDecimal("4056.00"));

    public static final Order ORDER_ENTITY_97 = new Order(97L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_46,
                                                          LocalDateTime.parse("2022-03-09T04:08:57.000"),
                                                          new BigDecimal("1227.00"));

    public static final Order ORDER_ENTITY_98 = new Order(98L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_40,
                                                          LocalDateTime.parse("2022-04-20T19:33:36.000"),
                                                          new BigDecimal("2124.00"));

    public static final Order ORDER_ENTITY_99 = new Order(99L,
                                                          USER_ENTITY_17,
                                                          GIFT_CERT_ENTITY_2,
                                                          LocalDateTime.parse("2023-02-08T15:25:05.523600"),
                                                          new BigDecimal("150.00"));

    public static final Order ORDER_ENTITY_100 = new Order(100L,
                                                           USER_ENTITY_17,
                                                           GIFT_CERT_ENTITY_29,
                                                           LocalDateTime.parse("2022-06-15T16:06:04.000"),
                                                           new BigDecimal("1899.00"));

    public static final Order ORDER_ENTITY_101 = new Order(101L,
                                                           USER_ENTITY_17,
                                                           GIFT_CERT_ENTITY_2,
                                                           LocalDateTime.parse("2023-01-03T15:25:05.523600"),
                                                           new BigDecimal("150.00"));

    public static final Order ORDER_ENTITY_102 = new Order(102L,
                                                           USER_ENTITY_17,
                                                           GIFT_CERT_ENTITY_47,
                                                           LocalDateTime.parse("2022-03-25T15:49:11.000"),
                                                           new BigDecimal("897.00"));

    public static final Order ORDER_ENTITY_103 = new Order(103L,
                                                           USER_ENTITY_17,
                                                           GIFT_CERT_ENTITY_19,
                                                           LocalDateTime.parse("2022-03-25T17:30:11.000"),
                                                           new BigDecimal("519.00"));

    public static final Order CREATED_ORDER_ENTITY = new Order(273L,
                                                               USER_ENTITY_1,
                                                               GIFT_CERT_ENTITY_42,
                                                               LocalDateTime.parse("2023-03-08T20:34:54.826"),
                                                               new BigDecimal("3382.00"));

    public static final List<Order> ORDER_98_TO_102_ENTITY_LIST
        = List.of(ORDER_ENTITY_98,
                  ORDER_ENTITY_99,
                  ORDER_ENTITY_100,
                  ORDER_ENTITY_101,
                  ORDER_ENTITY_102);

    public static final List<Order> ORDER_93_TO_97_ENTITY_LIST
        = List.of(ORDER_ENTITY_93,
                  ORDER_ENTITY_94,
                  ORDER_ENTITY_95,
                  ORDER_ENTITY_96,
                  ORDER_ENTITY_97);

    public static final List<Order> ORDER_99_TO_103_ENTITY_LIST
        = List.of(ORDER_ENTITY_99,
                  ORDER_ENTITY_100,
                  ORDER_ENTITY_101,
                  ORDER_ENTITY_102,
                  ORDER_ENTITY_103);

    public static final List<Order> ORDER_94_TO_98_ENTITY_LIST
        = List.of(ORDER_ENTITY_94,
                  ORDER_ENTITY_95,
                  ORDER_ENTITY_96,
                  ORDER_ENTITY_97,
                  ORDER_ENTITY_98);

    public static final List<Order> ORDER_93_TO_103_ENTITY_LIST
        = List.of(ORDER_ENTITY_93,
                  ORDER_ENTITY_94,
                  ORDER_ENTITY_95,
                  ORDER_ENTITY_96,
                  ORDER_ENTITY_97,
                  ORDER_ENTITY_98,
                  ORDER_ENTITY_99,
                  ORDER_ENTITY_100,
                  ORDER_ENTITY_101,
                  ORDER_ENTITY_102,
                  ORDER_ENTITY_103);

    private OrderEntityConstants() {
    }

    public static Order getOrderEntityToCreate() {
        return new Order(null,
                         USER_ENTITY_1,
                         GIFT_CERT_ENTITY_42,
                         null,
                         new BigDecimal("3382.00"));
    }
}
