package com.epam.esm.service.impl;

import static com.epam.esm.DTOConstants.OrderDTOConstants.CREATED_ORDER_DTO;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_93_TO_103_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_93_TO_97_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_94_TO_98_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_98_TO_102_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_99_TO_103_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_DTO_1;
import static com.epam.esm.DTOConstants.OrderDTOConstants.getOrderDTOToCreate;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_42;
import static com.epam.esm.EntityConstants.OrderEntityConstants.CREATED_ORDER_ENTITY;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_93_TO_103_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_93_TO_97_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_94_TO_98_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_98_TO_102_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_99_TO_103_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_ENTITY_1;
import static com.epam.esm.EntityConstants.OrderEntityConstants.getOrderEntityToCreate;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_ENTITY_1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.data.OrderRepository;
import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.Order;
import com.epam.esm.dto.OrderDTO;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchOrderException;
import com.epam.esm.service.OrderService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class OrderServiceImplTest {

    @Autowired
    OrderService orderService;
    @MockBean
    OrderRepository orderRepository;
    @MockBean
    UserRepository userRepository;
    @MockBean
    GiftCertificateRepository giftCertificateRepository;

    @Test
    void readOrder() {
        given(orderRepository.read(1)).willReturn(ORDER_ENTITY_1);
        assertEquals(ORDER_DTO_1, orderService.read(1L, 1L));
    }

    @Test
    void readWrongUserOrder() {
        given(orderRepository.read(1)).willReturn(ORDER_ENTITY_1);
        assertThrows(NoSuchOrderException.class, () -> orderService.read(42L, 1L));
    }

    @Test
    void createOrder() {
        given(userRepository.read(1L)).willReturn(USER_ENTITY_1);
        given(giftCertificateRepository.read(42L)).willReturn(GIFT_CERT_ENTITY_42);
        given(orderRepository.create(getOrderEntityToCreate())).willReturn(CREATED_ORDER_ENTITY);
        assertEquals(CREATED_ORDER_DTO, orderService.create(getOrderDTOToCreate()));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResult")
    void getOrdersList(OrderFilterParams orderFilterParams, List<OrderDTO> orderDTOList, List<Order> orderEntityList) {
        given(orderRepository.list(orderFilterParams)).willReturn(orderEntityList);
        assertEquals(orderDTOList, orderService.list(orderFilterParams));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResultAndPaging")
    void getPagingParams(OrderFilterParams orderFilterParams, List<OrderDTO> orderDTOList,
                         PagingParamsDTO pagingParamsDTO) {
        given(orderRepository.getNextAfterId(orderFilterParams,
                                             orderDTOList.get(orderDTOList.size() - 1).getId())).willReturn(
            pagingParamsDTO.getNextAfterId());
        given(orderRepository.getPrevAfterId(orderFilterParams, orderDTOList.get(0).getId())).willReturn(
            pagingParamsDTO.getPrevAfterId());
        given(orderRepository.getLastAfterId(orderFilterParams)).willReturn(
            pagingParamsDTO.getLastAfterId());
        assertEquals(pagingParamsDTO,
                     orderService.getPagingParams(orderFilterParams,
                                                  orderDTOList.get(orderDTOList.size() - 1).getId(),
                                                  orderDTOList.get(0).getId()));
    }

    private static Stream<Arguments> provideOrderPageParamsAndResult() {
        return Stream.of(Arguments.of(ORDER_NEXT_PAGE_FILTER_PARAMS,
                                      ORDER_98_TO_102_DTO_LIST,
                                      ORDER_98_TO_102_ENTITY_LIST),
                         Arguments.of(ORDER_FIRST_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_97_DTO_LIST,
                                      ORDER_93_TO_97_ENTITY_LIST),
                         Arguments.of(ORDER_LAST_PAGE_FILTER_PARAMS,
                                      ORDER_99_TO_103_DTO_LIST,
                                      ORDER_99_TO_103_ENTITY_LIST),
                         Arguments.of(ORDER_PREV_PAGE_FILTER_PARAMS,
                                      ORDER_94_TO_98_DTO_LIST,
                                      ORDER_94_TO_98_ENTITY_LIST),
                         Arguments.of(ORDER_EMPTY_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_103_DTO_LIST,
                                      ORDER_93_TO_103_ENTITY_LIST));
    }

    private static Stream<Arguments> provideOrderPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(ORDER_NEXT_PAGE_FILTER_PARAMS,
                                      ORDER_98_TO_102_DTO_LIST,
                                      ORDER_NEXT_PAGING),
                         Arguments.of(ORDER_FIRST_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_97_DTO_LIST,
                                      ORDER_FIRST_PAGING),
                         Arguments.of(ORDER_LAST_PAGE_FILTER_PARAMS,
                                      ORDER_99_TO_103_DTO_LIST,
                                      ORDER_LAST_PAGING),
                         Arguments.of(ORDER_PREV_PAGE_FILTER_PARAMS,
                                      ORDER_94_TO_98_DTO_LIST,
                                      ORDER_PREV_PAGING),
                         Arguments.of(ORDER_EMPTY_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_103_DTO_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class OrderServiceTestContextConfiguration {

        @Bean
        OrderService orderService(ModelMapper modelMapper, OrderRepository orderRepository,
                                  UserRepository userRepository,
                                  GiftCertificateRepository giftCertificateRepository) {
            return new OrderServiceImpl(modelMapper, orderRepository, userRepository, giftCertificateRepository);
        }

        @Bean
        ModelMapper modelMapper() {
            return new ModelMapper();
        }
    }

}