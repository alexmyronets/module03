package com.epam.esm.exception;

public class TagIsInUseException extends RuntimeException {

    public TagIsInUseException(String message) {
        super(message);
    }
}
