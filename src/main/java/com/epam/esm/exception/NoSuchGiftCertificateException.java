package com.epam.esm.exception;

public class NoSuchGiftCertificateException extends RuntimeException {

    public NoSuchGiftCertificateException(String message) {
        super(message);
    }
}
