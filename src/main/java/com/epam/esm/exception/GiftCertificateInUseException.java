package com.epam.esm.exception;

public class GiftCertificateInUseException extends RuntimeException {

    public GiftCertificateInUseException(String message) {
        super(message);
    }
}
