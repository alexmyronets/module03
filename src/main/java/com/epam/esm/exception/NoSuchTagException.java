package com.epam.esm.exception;

public class NoSuchTagException extends RuntimeException {

    public NoSuchTagException(String message) {
        super(message);
    }
}
