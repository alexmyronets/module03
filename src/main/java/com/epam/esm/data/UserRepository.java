package com.epam.esm.data;

import com.epam.esm.data.crud.ListPagingRepository;
import com.epam.esm.data.crud.ReadRepository;
import com.epam.esm.domain.User;

/**
 * This interface consists of methods for User persistence operations
 *
 * @author Oleksandr Myronets.
 */
public interface UserRepository extends ReadRepository<User>, ListPagingRepository<User> {

}
