package com.epam.esm.data.impl;

import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.exception.NoSuchUserException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final Logger LOGGER = LogManager.getLogger(UserRepositoryImpl.class);

    private final EntityManager entityManager;

    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User read(long id) {
        LOGGER.info("Querying User by id = {} from DB", id);
        User user = entityManager.find(User.class, id);
        if (user == null) {
            LOGGER.debug("User with id = {} doesn't exist", id);
            throw new NoSuchUserException("Requested resource not found (id = " + id + ")");
        }
        LOGGER.info("Returning requested User with id = {}", id);
        return user;
    }

    @Override
    public List<User> list(FilterParams filterParams) {
        LOGGER.info("Querying and returning List of all users from DB");
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user);

        if (filterParams.getPage().getAfterId() != null) {
            LOGGER.debug("Pagination criteria is present, need to consider in query");
            return getUserList(filterParams, cb, query, user);
        }
        LOGGER.debug("Pagination criteria is absent, querying first page");
        query.orderBy(cb.asc(user));
        LOGGER.info("Returning List of Users");
        return entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize()).getResultList();
    }

    @Override
    public Long getNextAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is next page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).where(cb.gt(user.get("id"), candidate)).orderBy(cb.asc(user));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Next page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Next page doesn't exist");
        return -1L;
    }

    @Override
    public Long getPrevAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is previous page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).where(cb.lt(user.get("id"), candidate)).orderBy(cb.desc(user));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Previous page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Previous page doesn't exist");
        return -1L;
    }

    @Override
    public Long getLastAfterId(FilterParams filterParams) {
        LOGGER.info("Check whether there are more than one page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).orderBy(cb.desc(user));
        List<User> users = entityManager.createQuery(query)
                                        .setFirstResult(filterParams.getPage().getSize())
                                        .setMaxResults(filterParams.getPage().getSize() + 1)
                                        .getResultList();
        if (!users.isEmpty()) {
            long afterId = users.get(0).getId();
            LOGGER.info("There are more than one page, last page after id: {}", afterId);
            return afterId;
        }
        LOGGER.info("There is no more than one page");
        return -1L;
    }

    private List<User> getUserList(FilterParams filterParams, CriteriaBuilder cb, CriteriaQuery<User> query,
                                   Root<User> user) {
        LOGGER.debug("Retrieving List of Users from DB by paging criteria");
        if (filterParams.getPage().getDirection() == PageDirection.FORWARD) {
            query.where(cb.gt(user.get("id"), filterParams.getPage().getAfterId())).orderBy(cb.asc(user.get("id")));
            return entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize()).getResultList();
        }
        query.where(cb.lt(user.get("id"), filterParams.getPage().getAfterId())).orderBy(cb.desc(user.get("id")));
        List<User> users = entityManager.createQuery(query)
                                        .setMaxResults(filterParams.getPage().getSize())
                                        .getResultList();
        LOGGER.info("Page direction is BACK reversing result");
        Collections.reverse(users);
        return users;
    }
}
