package com.epam.esm.data.impl;

import static com.epam.esm.data.DBConstants.JPQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER;

import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class TagRepositoryImpl implements TagRepository {

    protected static final Logger LOGGER = LogManager.getLogger(TagRepositoryImpl.class);

    private final EntityManager entityManager;

    public TagRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Tag create(Tag tag) {
        LOGGER.info("Saving tag {} into DB", tag);

        if (isTagExists(tag.getName())) {
            LOGGER.debug("Tag with name {}, already exists", tag::getName);
            throw new TagAlreadyExistsException("Tag (name = " + tag.getName() + ") is already exists");
        }
        entityManager.persist(tag);
        LOGGER.info("Returning just created Tag");
        return tag;
    }

    @Override
    public Tag read(long id) {
        LOGGER.info("Querying Tag by id = {} from DB", id);
        Tag tag = entityManager.find(Tag.class, id);

        if (tag == null) {
            LOGGER.debug("Tag with id = {} doesn't exist", id);
            throw new NoSuchTagException("Requested resource not found (id = " + id + ")");
        }
        LOGGER.info("Returning requested Tag with id: {}", id);
        return tag;
    }

    @Override
    @Transactional
    public void delete(long id) {
        LOGGER.info("Removing Tag with id = {} from DB", id);
        Tag tag = read(id);
        int certCount = countCertificatesByTag(id);
        if (certCount > 0) {
            LOGGER.debug("Tag with id = {} cannot be deleted, because it in use by {} Gift Certificates",
                         id,
                         certCount);
            throw new TagIsInUseException("Tag (id = " + id + ") is in use by " + certCount + " gift certificates");
        }
        entityManager.remove(tag);
        LOGGER.info("Tag with id = {} removed from DB", id);
    }

    @Override
    public List<Tag> list(FilterParams filterParams) {
        LOGGER.info("Querying and returning List of tags from DB");
        TypedQuery<Tag> typedQuery;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag);

        if (filterParams.getPage().getAfterId() != null) {
            LOGGER.debug("Pagination criteria is present, need to consider in query");
            return getTagList(filterParams, cb, query, tag);
        }
        LOGGER.debug("Pagination criteria is absent, querying first page");
        query.orderBy(cb.asc(tag));
        typedQuery = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize());
        LOGGER.info("Returning List of Tags");
        return typedQuery.getResultList();
    }


    @Override
    public Long getNextAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is next page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag).where(cb.gt(tag.get("id"), candidate)).orderBy(cb.asc(tag));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Next page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Next page doesn't exist");
        return -1L;
    }

    @Override
    public Long getPrevAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is previous page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag);
        query.where(cb.lt(tag.get("id"), candidate));
        query.orderBy(cb.desc(tag));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Previous page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Previous page doesn't exist");
        return -1L;
    }

    @Override
    public Long getLastAfterId(FilterParams filterParams) {
        LOGGER.info("Check whether there are more than one page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag);
        query.orderBy(cb.desc(tag));

        List<Tag> tags = entityManager.createQuery(query)
                                      .setFirstResult(filterParams.getPage().getSize())
                                      .setMaxResults(filterParams.getPage().getSize() + 1)
                                      .getResultList();

        if (!tags.isEmpty()) {
            long afterId = tags.get(0).getId();
            LOGGER.info("There are more than one page, last page after id: {}", afterId);
            return afterId;
        }
        LOGGER.info("There is no more than one page");
        return -1L;
    }

    @Override
    public Tag readMostUsedTagByMostSpendUser() {
        LOGGER.info("Querying most used Tag by most spent user from DB");

        //Criteria query way
        /*CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
        Root<Order> userQueryRoot = userQuery.from(Order.class);
        Join<Order, User> user = userQueryRoot.join("user");
        userQuery.select(user)
                 .groupBy(user)
                 .orderBy(cb.desc(cb.sum(userQueryRoot.get("orderCost"))));
        User mostOrdersCostUser = entityManager.createQuery(userQuery)
                                               .setMaxResults(1)
                                               .getSingleResult();

        CriteriaQuery<Tag> tagQuery = cb.createQuery(Tag.class);
        Root<Order> tagQueryroot = tagQuery.from(Order.class);
        Join<Order, GiftCertificate> giftCertificate = tagQueryroot.join("giftCertificate");
        Join<GiftCertificate, Tag> tag = giftCertificate.join("tags");
        tagQuery.select(tag)
                .where(cb.equal(tagQueryroot.get("user"), mostOrdersCostUser))
                .groupBy(tag)
                .orderBy(cb.desc(cb.count(giftCertificate)));
        List<Tag> tagList = entityManager.createQuery(tagQuery).setMaxResults(1).getResultList();*/

        //Native Hibernate way
        /*HibernateCriteriaBuilder cb = entityManager.unwrap(Session.class).getCriteriaBuilder();
        JpaCriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
        JpaRoot<Order> order = cq.from(Order.class);
        Join<Order, GiftCertificate> giftCertificate = order.join("giftCertificate");
        Join<GiftCertificate, Tag> tag = giftCertificate.join("tags");
        Join<Order, User> user = order.join("user");

        //sub-query to find user's orders sums
        JpaSubQuery<Tuple> querySumOfOrders = cq.subquery(Tuple.class);
        JpaRoot<Order> subOrderSumOfOrders = querySumOfOrders.from(Order.class);
        querySumOfOrders.multiselect(cb.sum(subOrderSumOfOrders.get("orderCost")).alias("theSum"))
                        .groupBy(subOrderSumOfOrders.get("user"));

        //sub-query to find max cost of the user's orders sum
        JpaSubQuery<BigDecimal> queryMaxOrdersSum = cq.subquery(BigDecimal.class);
        JpaRoot<Tuple> queryMaxOrders = queryMaxOrdersSum.from(querySumOfOrders);
        queryMaxOrdersSum.select(cb.max(queryMaxOrders.get("theSum")));

        //sub-query to find user with most cost of all orders
        JpaSubQuery<User> queryMostSpentUser = cq.subquery(User.class);
        JpaRoot<Order> subOrderMostSpentUser = queryMostSpentUser.from(Order.class);
        Join<Order, User> subUserMostSpentUser = subOrderMostSpentUser.join("user");
        queryMostSpentUser.select(subUserMostSpentUser)
                          .having(cb.equal(cb.sum(subOrderMostSpentUser.get("orderCost")), cb.max(queryMaxOrdersSum)))
                          .groupBy(subUserMostSpentUser);
        //main query
        cq.select(tag)
          .where(cb.equal(user, queryMostSpentUser))
          .orderBy(cb.desc(cb.count(giftCertificate)))
          .groupBy(tag);
        List<Tag> tagList = entityManager.createQuery(cq)
                                         .setMaxResults(1)
                                         .getResultList();*/

        //JPQL way
        List<Tag> tagList = entityManager.createQuery(JPQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER, Tag.class)
                                         .setMaxResults(1)
                                         .getResultList();

        //Native SQL way
        /*@SuppressWarnings("unchecked")
        List<Tag> tagList = entityManager.createNativeQuery(SQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER, Tag.class).setMaxResults(1).getResultList();*/

        if (tagList.isEmpty()) {
            LOGGER.debug("Most used Tag by most spent user doesn't exist.");
            throw new NoSuchTagException("Requested resource not found");
        }
        LOGGER.info("Returning most used Tag by most spent user");
        return tagList.get(0);

    }


    private int countCertificatesByTag(long tagId) {
        LOGGER.debug("Counting amount of Gift Certificates for Tag with id = {}", tagId);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<GiftCertificate> giftCertificate = cq.from(GiftCertificate.class);
        Join<GiftCertificate, Tag> tag = giftCertificate.join("tags");

        cq.select(cb.count(giftCertificate)).where(cb.equal(tag.get("id"), tagId));

        TypedQuery<Long> query = entityManager.createQuery(cq);

        return Math.toIntExact(query.getSingleResult());

        //JPQL way
        /*String jpql = "SELECT COUNT(g) FROM GiftCertificate g JOIN g.tags t WHERE t.id = :tagId";
        TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        query.setParameter("tagId", id);
        return Math.toIntExact(query.getSingleResult());*/
    }

    private boolean isTagExists(String name) {
        LOGGER.debug("Querying whether Tag with name = {} exists", name);

        //Criteria Query way
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
        Root<Tag> tag = cq.from(Tag.class);
        cq.select(tag).where(cb.equal(tag.get("name"), name));
        TypedQuery<Tag> query = entityManager.createQuery(cq);

        //JPQL way
        /*TypedQuery<Tag> query = entityManager.createQuery("SELECT t FROM Tag t WHERE t.name = :name", Tag.class);
        query.setParameter("name", name);*/

        List<Tag> results = query.getResultList();
        return !results.isEmpty();
    }

    private List<Tag> getTagList(FilterParams filterParams, CriteriaBuilder cb, CriteriaQuery<Tag> query,
                                 Root<Tag> tag) {
        LOGGER.debug("Retrieving List of Tags from DB by paging criteria");
        TypedQuery<Tag> typedQuery;
        if (filterParams.getPage().getDirection() == PageDirection.FORWARD) {
            query.where(cb.gt(tag.get("id"), filterParams.getPage().getAfterId()));
            query.orderBy(cb.asc(tag.get("id")));
            typedQuery = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize());
            return typedQuery.getResultList();
        }

        query.where(cb.lt(tag.get("id"), filterParams.getPage().getAfterId()));
        query.orderBy(cb.desc(tag.get("id")));
        typedQuery = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize());
        List<Tag> result = typedQuery.getResultList();
        LOGGER.info("Page direction is BACK reversing result");
        Collections.reverse(result);
        return result;
    }
}
