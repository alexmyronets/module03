package com.epam.esm.data.crud;

import com.epam.esm.dto.FilterParams;
import java.util.List;

public interface ListPagingRepository<T> {

    List<T> list(FilterParams filterParams);

    Long getNextAfterId(FilterParams filterParams, Long candidate);

    Long getPrevAfterId(FilterParams filterParams, Long candidate);

    Long getLastAfterId(FilterParams filterParams);

}
