package com.epam.esm.data.crud;

public interface DeleteRepository<T> {

    void delete(long id);

}
