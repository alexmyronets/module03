package com.epam.esm.data.crud;


/**
 * Repository interface for Create operation
 */
public interface CreateRepository<T> {

    /**
     * Creates and returns created object
     *
     * @param t object to create
     * @return created object
     */
    T create(T t);
}
