package com.epam.esm;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Module03Application {

    private static final Logger LOGGER = LogManager.getLogger(Module03Application.class);

    public static void main(String[] args) {
        LOGGER.info("Starting app");
        SpringApplication.run(Module03Application.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
