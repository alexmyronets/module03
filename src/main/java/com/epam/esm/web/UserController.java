package com.epam.esm.web;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.UserDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.hateoas.PagedModel;
import com.epam.esm.service.UserService;
import com.google.common.collect.Iterables;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class consists of methods for managing requests for read operation of {@link User}.
 *
 * @author Oleksandr Myronets
 */
@RestController
@RequestMapping("/api/users")
@Validated
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);

    private final UserService userService;

    /**
     * Constructs User Controller
     *
     * @param userService {@link UserService}
     */
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Performs list operation for User.
     *
     * @param filterParams parameters of filter
     * @return List of {@link UserDTO} in JSON format.
     */
    @GetMapping(produces = HAL_JSON_VALUE)
    public CollectionModel<UserDTO> getUsersList(@Valid FilterParams filterParams) {
        LOGGER.info("GET request for the list of users");
        List<UserDTO> users = userService.list(filterParams);
        return getUsersWithHATEOAS(filterParams, users);
    }

    /**
     * Performs read operation for User.
     *
     * @param id id of User to read.
     * @return {@link UserDTO} in JSON format.
     */
    @GetMapping(value = "/{id}", produces = HAL_JSON_VALUE)
    public UserDTO getUser(
        @PathVariable @Min(value = 1, message = "Id should be greater than 0") Long id) {
        LOGGER.info("GET request for user with id = {}", id);
        UserDTO userDTO = userService.read(id);
        addHATEOASLinks(userDTO);
        return userDTO;
    }

    private static void addHATEOASLinks(UserDTO user) {
        LOGGER.info("Adding HATEOAS links for each User");
        Link selfLink = linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel();
        Link ordersLink = linkTo(methodOn(OrderController.class).
                                     getOrdersList(user.getId(),
                                                   new OrderFilterParams())).withRel("orders");
        user.add(selfLink);
        user.add(ordersLink);
    }

    private static URI getBaseURI(FilterParams filterParams) {
        LOGGER.info("Composing and returning base URI for request");
        return linkTo(methodOn(UserController.class).getUsersList(filterParams)).toUri();
    }

    private CollectionModel<UserDTO> getUsersWithHATEOAS(FilterParams filterParams, List<UserDTO> users) {
        LOGGER.info("Adding HATEOAS links for List of Users");
        URI baseURI = getBaseURI(filterParams);
        if (!users.isEmpty()) {
            PagingParamsDTO pagingParamsDTO = userService.getPagingParams(filterParams,
                                                                          Iterables.getLast(users).getId(),
                                                                          users.get(0).getId());
            users.forEach(UserController::addHATEOASLinks);
            LOGGER.info("Return List of Users with added HATEOAS links");
            return PagedModel.of(users, baseURI, filterParams.getPage(), pagingParamsDTO);
        }
        LOGGER.debug("List is empty, need to add only self link");
        LOGGER.info("Return empty List with self link");
        return PagedModel.empty(baseURI, filterParams.getPage());
    }
}
