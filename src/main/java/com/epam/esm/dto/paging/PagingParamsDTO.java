package com.epam.esm.dto.paging;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PagingParamsDTO {

    private Long nextAfterId;
    private Long prevAfterId;
    private Long lastAfterId;
}
