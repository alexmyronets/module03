package com.epam.esm.dto.paging;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5425601170800357466L;

    @Min(value = 5, message = "Minimum number of items per page is 5")
    @Max(value = 100, message = "Maximum number of items per page is 100")
    private Byte size = 50;

    @Min(value = 1, message = "Id cannot be less than one")
    private Long afterId;

    private PageDirection direction = PageDirection.FORWARD;
}
