package com.epam.esm.dto.paging;

public enum PageDirection {
    BACK, FORWARD

}
