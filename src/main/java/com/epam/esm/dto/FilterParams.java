package com.epam.esm.dto;

import com.epam.esm.dto.paging.PageDTO;
import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterParams implements Serializable {

    @Serial
    private static final long serialVersionUID = 1258211413967958745L;

    private PageDTO page = new PageDTO();
}
