package com.epam.esm.dto;

public enum SortOrder {
    ASC, DESC
}
