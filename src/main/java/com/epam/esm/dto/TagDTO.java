package com.epam.esm.dto;

import com.epam.esm.validation.PatchInfo;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.Default;
import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Relation(collectionRelation = "tags", itemRelation = "tag")
public class TagDTO extends RepresentationModel<TagDTO> implements Serializable {

    @Serial
    private static final long serialVersionUID = -7877282820567241972L;

    private Long id;

    @NotNull(message = "Tag name is mandatory", groups = {Default.class, PatchInfo.class})
    @Size(min = 3, max = 15, message = "Tag name cannot be shorter than 3 characters and longer than 15 characters", groups = {
        Default.class,
        PatchInfo.class})
    private String name;
}
