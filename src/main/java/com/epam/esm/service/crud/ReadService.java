package com.epam.esm.service.crud;

/**
 * Service interface for Read operation
 */
public interface ReadService<T> {

    /**
     * Reads object
     *
     * @param id of the object
     * @return read object
     */
    T read(long id);

}
