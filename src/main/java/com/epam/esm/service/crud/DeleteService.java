package com.epam.esm.service.crud;

/**
 * Service interface for Delete operation
 */
public interface DeleteService {

    /**
     * Deletes object
     *
     * @param id of object to delete
     */
    void delete(long id);

}
