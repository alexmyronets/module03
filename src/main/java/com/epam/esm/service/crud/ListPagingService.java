package com.epam.esm.service.crud;

import com.epam.esm.data.crud.ListPagingRepository;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service interface for List operation with pagination
 */
public interface ListPagingService<T> {

    Logger LOGGER = LogManager.getLogger(ListPagingService.class);

    /**
     * Reads list of objects
     *
     * @param filterParams parameters of filtering
     * @return read {@link List} of objects
     */
    List<T> list(FilterParams filterParams);

    /**
     * Gets metadata for pagination
     *
     * @param filterParams  parameters of filtering
     * @param nextCandidate candidate id for next page
     * @param prevCandidate candidate id for previous page
     * @return {@link PagingParamsDTO}
     */
    PagingParamsDTO getPagingParams(FilterParams filterParams, Long nextCandidate, Long prevCandidate);

    /**
     * Gets metadata for pagination
     *
     * @param filterParams         parameters of filtering
     * @param nextCandidate        candidate id for next page
     * @param prevCandidate        candidate id for previous page
     * @param listPagingRepository {@link ListPagingRepository}
     * @return {@link PagingParamsDTO}
     */
    default PagingParamsDTO getPagingParams(FilterParams filterParams, Long nextCandidate, Long prevCandidate,
                                            ListPagingRepository<?> listPagingRepository) {
        LOGGER.info("Get Paging params method");
        Long nextId;
        Long prevId;
        Long lastId;

        LOGGER.trace("Creating empty PagingParamsDTO");
        PagingParamsDTO pagingParamsDTO = new PagingParamsDTO();

        LOGGER.debug("Getting next id");
        nextId = listPagingRepository.getNextAfterId(filterParams, nextCandidate);
        LOGGER.debug("Next id is: {}", nextId);

        LOGGER.debug("Getting previous id");
        prevId = listPagingRepository.getPrevAfterId(filterParams, prevCandidate);
        LOGGER.debug("Previous id is: {}", prevId);

        LOGGER.debug("Getting last id");
        lastId = listPagingRepository.getLastAfterId(filterParams);
        LOGGER.debug("Last id is: {}", lastId);

        pagingParamsDTO.setNextAfterId(nextId);
        pagingParamsDTO.setPrevAfterId(prevId);
        pagingParamsDTO.setLastAfterId(lastId);

        LOGGER.info("Returning paging params");
        return pagingParamsDTO;
    }
}
