package com.epam.esm.service.crud;

/**
 * Service interface of Create operation
 */
public interface CreateService<T> {

    /**
     * Creates and returns created object
     *
     * @param t object to create
     * @return created object
     */
    T create(T t);

}
