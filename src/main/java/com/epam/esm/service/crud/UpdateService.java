package com.epam.esm.service.crud;

/**
 * Service interface for Update operation
 */
public interface UpdateService<T> {

    /**
     * @param t  object containing update operation
     * @param id of object to update
     * @return updated object
     */
    T update(T t, long id);

}
