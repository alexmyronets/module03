package com.epam.esm.service;

import com.epam.esm.domain.Tag;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.service.crud.CreateService;
import com.epam.esm.service.crud.DeleteService;
import com.epam.esm.service.crud.ListPagingService;
import com.epam.esm.service.crud.ReadService;

/**
 * This interface consists of methods for mapping between {@link TagDTO} and {@link Tag} and provides pagination
 * metadata for list operation.
 *
 * @author Oleksandr Myronets.
 */
public interface TagService extends CreateService<TagDTO>, ReadService<TagDTO>, DeleteService,
    ListPagingService<TagDTO> {

    /**
     * Finds most used Tag among orders of most spend user
     *
     * @return {@link TagDTO}
     */
    TagDTO readMostUsedTagByMostSpendUser();
}
