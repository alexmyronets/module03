package com.epam.esm.service;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.service.crud.CreateService;
import com.epam.esm.service.crud.DeleteService;
import com.epam.esm.service.crud.ListPagingService;
import com.epam.esm.service.crud.ReadService;
import com.epam.esm.service.crud.UpdateService;

/**
 * This interface consists of methods for mapping between {@link GiftCertificateDTO} and {@link GiftCertificate} and
 * provides pagination metadata for list operation.
 *
 * @author Oleksandr Myronets.
 */
public interface GiftCertificateService extends CreateService<GiftCertificateDTO>,
    ReadService<GiftCertificateDTO>,
    UpdateService<GiftCertificateDTO>, DeleteService,
    ListPagingService<GiftCertificateDTO> {

}
