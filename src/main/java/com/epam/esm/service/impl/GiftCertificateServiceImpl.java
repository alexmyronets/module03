package com.epam.esm.service.impl;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.service.GiftCertificateService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

/**
 * @author Oleksandr Myronets.
 */
@Service
public class GiftCertificateServiceImpl implements GiftCertificateService {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateServiceImpl.class);

    private final GiftCertificateRepository giftCertificateRepository;

    private final ModelMapper modelMapper;

    public GiftCertificateServiceImpl(GiftCertificateRepository giftCertificateRepository, ModelMapper modelMapper) {
        this.giftCertificateRepository = giftCertificateRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public GiftCertificateDTO create(GiftCertificateDTO giftCertDTO) {
        LOGGER.info("Create Gift Certificate method");
        LOGGER.debug("Mapping GiftCertificateDTO: {} to GiftCertificate for creation", giftCertDTO);
        GiftCertificate giftCertificateToCreate = modelMapper.map(giftCertDTO, GiftCertificate.class);
        GiftCertificate createdGiftCertificate = giftCertificateRepository.create(giftCertificateToCreate);

        LOGGER.debug("Mapping created GiftCertificate: {} to GiftCertificateDTO", createdGiftCertificate);
        GiftCertificateDTO createdGiftCertificateDTO = modelMapper.map(createdGiftCertificate,
                                                                       GiftCertificateDTO.class);
        LOGGER.info("Returning mapped Gift Certificate DTO: {}", createdGiftCertificateDTO);
        return createdGiftCertificateDTO;
    }

    @Override
    public GiftCertificateDTO read(long id) {
        LOGGER.info("Read Gift Certificate method");
        GiftCertificate giftCertificate = giftCertificateRepository.read(id);

        LOGGER.debug("Mapping read GiftCertificate: {} to GiftCertificateDTO", giftCertificate);
        GiftCertificateDTO giftCertificateDTO = modelMapper.map(giftCertificate, GiftCertificateDTO.class);
        LOGGER.debug("Returning mapped Gift Certificate DTO: {}", giftCertificateDTO);
        return giftCertificateDTO;
    }

    @Override
    public GiftCertificateDTO update(GiftCertificateDTO giftCertDTO, long id) {
        LOGGER.info("Update Gift Certificate method");
        LOGGER.debug("Mapping GiftCertificateDTO: {} to GiftCertificate for update", giftCertDTO);
        GiftCertificate giftCertificateToUpdate = modelMapper.map(giftCertDTO, GiftCertificate.class);
        GiftCertificate updatedGiftCertificate = giftCertificateRepository.update(giftCertificateToUpdate, id);

        LOGGER.debug("Mapping updated GiftCertificate: {} to GiftCertificateDTO", updatedGiftCertificate);
        LOGGER.info("Returning updated Gift Certificate");
        return modelMapper.map(updatedGiftCertificate, GiftCertificateDTO.class);
    }

    @Override
    public void delete(long id) {
        LOGGER.info("Delete Gift Certificate method");
        LOGGER.debug("Deleting Gift Certificate with id: {}", id);
        giftCertificateRepository.delete(id);
    }

    @Override
    public List<GiftCertificateDTO> list(FilterParams filterParams) {
        LOGGER.info("List Gift Certificate method");
        LOGGER.debug("Getting List of Gift Certificates by parameters: {}", filterParams);
        List<GiftCertificate> giftCertificates = giftCertificateRepository.list(filterParams);
        LOGGER.debug("Mapping list of Gift Certificates to Gift Certificates DTO List: {}", giftCertificates);
        List<GiftCertificateDTO> giftCertificatesDTO = giftCertificates.stream().map(giftCertificate -> modelMapper.map(
            giftCertificate,
            GiftCertificateDTO.class)).toList();
        LOGGER.info("Mapped Gift Certificate DTO List: {}", giftCertificatesDTO);
        LOGGER.info("Returning Gift Certificate DTO List");
        return giftCertificatesDTO;
    }

    @Override
    public PagingParamsDTO getPagingParams(FilterParams filterParams, Long nextCandidate, Long prevCandidate) {
        return getPagingParams(filterParams, nextCandidate, prevCandidate, giftCertificateRepository);
    }
}
