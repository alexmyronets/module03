package com.epam.esm.hateoas;

import com.epam.esm.dto.paging.PageDTO;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.dto.paging.PagingParamsDTO;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * DTO to implement binding response representations of pageable collections.
 *
 * @param <T>
 */
public class PagedModel<T> extends CollectionModel<T> {

    private static final Logger LOGGER = LogManager.getLogger(PagedModel.class);

    /**
     * Creates a {@link CollectionModel} with the given content and pagination links composed of parameters data.
     *
     * @param content      {@link Collection} to create pagination link for
     * @param baseURI      base {@link URI} of request
     * @param pageParams   {@link PageDTO} containing details of the page
     * @param pagingParams {@link PagingParamsDTO} containing details of pagination
     * @param <T>
     * @return {@link Iterable} with pagination links
     */
    public static <T> CollectionModel<T> of(Collection<T> content, URI baseURI, PageDTO pageParams,
                                            PagingParamsDTO pagingParams) {
        LOGGER.info("Starting to compose paging links list");

        LOGGER.debug("Creating links List");
        List<Link> links = new LinkedList<>();

        LOGGER.debug("Adding first page link to links List");
        links.add(getFirstPageLink(baseURI, pageParams.getSize()));

        if (pagingParams.getPrevAfterId() != -1) {
            LOGGER.debug("Adding previous page link to links List");
            links.add(getPrevPageLink(baseURI, pageParams.getSize(), pagingParams));
        }
        LOGGER.debug("Adding self page link to links List");
        links.add(getSelfPageLink(baseURI, pageParams));

        if (pagingParams.getNextAfterId() != -1) {
            LOGGER.debug("Adding next page link to links List");
            links.add(getNextPageLink(baseURI, pageParams.getSize(), pagingParams));
        }

        if (pagingParams.getLastAfterId() != -1) {
            LOGGER.debug("Adding last page link to links List");
            links.add(getLastPageLink(baseURI, pageParams.getSize(), pagingParams));
        }

        LOGGER.info("Returning composed links List: {}", links);

        return CollectionModel.of(content, links);
    }

    /**
     * Creates an empty {@link CollectionModel} with self link composed of parameters data.
     *
     * @param baseURI    base {@link URI} of request
     * @param pageParams {@link PageDTO} containing details of the page
     * @param <T>
     * @return empty {@link List} with self link
     */
    public static <T> CollectionModel<T> empty(URI baseURI, PageDTO pageParams) {
        LOGGER.info("Composing only self link for empty result");
        Link selflink = getSelfPageLink(baseURI, pageParams);
        LOGGER.info("Returning links List for empty result");
        return CollectionModel.of(Collections.emptyList(), selflink);
    }

    private static Link getFirstPageLink(URI baseURI, Byte pageSize) {
        LOGGER.debug("Composing first page link");
        var uri = UriComponentsBuilder.fromUri(baseURI)
                                      .queryParam("page.size", pageSize)
                                      .build();
        LOGGER.trace("First page link uri: {}", uri);
        LOGGER.debug("Returning first page link");
        return Link.of(uri.toString(), "first");
    }

    private static Link getPrevPageLink(URI baseURI, Byte pageSize, PagingParamsDTO pagingParams) {
        LOGGER.debug("Composing previous page link");
        var uri = UriComponentsBuilder.fromUri(baseURI)
                                      .queryParam("page.size", pageSize)
                                      .queryParam("page.afterId", pagingParams.getPrevAfterId())
                                      .queryParam("page.direction", PageDirection.BACK.name())
                                      .build();
        LOGGER.trace("Previous page link uri: {}", uri);
        LOGGER.debug("Returning previous page link");
        return Link.of(uri.toString(), "prev");
    }

    private static Link getSelfPageLink(URI baseURI, PageDTO pageParams) {
        LOGGER.debug("Composing self page link");
        var uri = UriComponentsBuilder.fromUri(baseURI)
                                      .queryParam("page.size", pageParams.getSize());
        if (pageParams.getAfterId() != null) {
            uri.queryParam("page.afterId", pageParams.getAfterId())
               .queryParam("page.direction", pageParams.getDirection());
        }
        LOGGER.trace("Self page link uri: {}", uri);
        LOGGER.debug("Returning self page link");
        return Link.of(uri.build()
                          .toString(), "self");
    }

    private static Link getNextPageLink(URI baseURI, Byte pageSize, PagingParamsDTO pagingParams) {
        LOGGER.debug("Composing next page link");
        var uri = UriComponentsBuilder.fromUri(baseURI)
                                      .queryParam("page.size", pageSize)
                                      .queryParam("page.afterId", pagingParams.getNextAfterId())
                                      .queryParam("page.direction", PageDirection.FORWARD.name())
                                      .build();

        LOGGER.trace("Next page link uri: {}", uri);
        LOGGER.debug("Returning next page link");
        return Link.of(uri.toString(), "next");
    }

    private static Link getLastPageLink(URI baseURI, Byte pageSize, PagingParamsDTO pagingParams) {
        LOGGER.debug("Composing last page link");
        var uri = UriComponentsBuilder.fromUri(baseURI)
                                      .queryParam("page.size", pageSize)
                                      .queryParam("page.afterId", pagingParams.getLastAfterId())
                                      .queryParam("page.direction", PageDirection.FORWARD.name())
                                      .build();

        LOGGER.trace("Last page link uri: {}", uri);
        LOGGER.debug("Returning last page link");
        return Link.of(uri.toString(), "last");
    }


}
