plugins {
	java
	id("org.springframework.boot") version "3.0.1"
	id("io.spring.dependency-management") version "1.1.0"
}

group = "com.epam.esm"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
	implementation {
		exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.zaxxer:HikariCP")
	implementation("org.postgresql:postgresql")
	implementation("com.google.code.findbugs:jsr305:3.0.2")
	implementation("org.apache.logging.log4j:log4j-core")
	implementation("org.apache.logging.log4j:log4j-api")
	implementation("org.modelmapper:modelmapper:3.1.1")
	implementation("com.h2database:h2:2.1.214")
	implementation("org.springframework.boot:spring-boot-starter-hateoas")
	implementation ("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("com.google.guava:guava:31.1-jre")
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
}
